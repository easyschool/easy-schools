/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */

 var authorize = require('../api/policies/authorize');
 var CONSTANTS = require('./constants.js').constants;
module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions (`true` allows public     *
  * access)                                                                  *
  *                                                                          *
  ***************************************************************************/

  '*': 'localize',
  SchoolsController: {
    '*': ['localize', 'sessionAuth'],
    'renderSchools' :  ['localize', authorize([CONSTANTS.userRoles.individuals.superuser])],
    'renderNewSchool' : ['localize',authorize([CONSTANTS.userRoles.individuals.superuser])],
    'renderRemoveSchool': ['localize', authorize([CONSTANTS.userRoles.individuals.superuser])],
    'removeSchool' : ['localize', authorize([CONSTANTS.userRoles.individuals.superuser])],
    'createNewSchool' : ['localize', authorize([CONSTANTS.userRoles.individuals.superuser])],
    'removeSchool' : ['localize', authorize([CONSTANTS.userRoles.individuals.superuser])],
    'renderSchool' : ['localize',authorize(CONSTANTS.userRoles.group1Roles)],
    'renderUpdateSchool' : ['localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'updateSchool' : ['localize', authorize(CONSTANTS.userRoles.group1Roles)]
  },
  ClassesController: {
    '*': ['sessionAuth', 'localize'],
    'renderClass': ['localize'],
    'renderClassesNew' : ['localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'renderClassesRemove' : ['localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'createClass' : ['localize',authorize(CONSTANTS.userRoles.group1Roles)],
    'removeClass' : ['localize',authorize(CONSTANTS.userRoles.group1Roles)],
    'updateClass' : ['localize',authorize(CONSTANTS.userRoles.group1Roles)],
    'removeClass' : ['localize',authorize(CONSTANTS.userRoles.group1Roles)],
    'updateClass' : ['localize',authorize(CONSTANTS.userRoles.group1Roles)]
  },
  UsersController: {
    'renderUsers': ['sessionAuth', 'localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'renderUsersNew': ['sessionAuth', 'localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'renderUsersSubject': ['sessionAuth', 'localize', authorize(CONSTANTS.userRoles.group3Roles)],
    'renderUserUpdate': ['sessionAuth', 'localize'],
    'renderMenu': ['sessionAuth', 'localize'],
    'logout': ['sessionAuth', 'localize'],
    'createUser': ['sessionAuth', 'localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'updateUser': ['sessionAuth', 'localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'search': ['sessionAuth', 'localize', authorize(CONSTANTS.userRoles.group2Roles)],
    'renderUserHomePage' : ['sessionAuth', 'localize'],
    'renderPatchInsert' : ['sessionAuth', 'localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'patchInsertUsers' : ['sessionAuth', 'localize', authorize(CONSTANTS.userRoles.group1Roles)]
    },
  DivisionController: {
     '*': ['sessionAuth', 'localize'],
    'renderDivsion' : ['localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'renderDivsionNew' : ['localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'createDivision' : ['localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'updateDivison' : ['localize',authorize(CONSTANTS.userRoles.group1Roles)],
    'deleteDivision': ['localize', authorize([CONSTANTS.userRoles.individuals.superuser])]
  },
  SubjectsController: {
    // '*': ['sessionAuth', 'localize'],
    'renderSubjectsList' : ['localize', authorize(CONSTANTS.userRoles.group2Roles)],
    'renderSubjectsNew' : ['localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'createSubject' : ['localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'renderSubjectUpdate' : ['localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'deleteSubject' : ['localize', authorize(CONSTANTS.userRoles.group1Roles)],
    'renderSubjects' : ['localize', authorize(CONSTANTS.userRoles.group3Roles)],
    'renderSubjectUpdate' : ['localize', authorize(CONSTANTS.userRoles.group1Roles)]
  },
  HomeworkController: {
    // '*': ['sessionAuth', 'localize'],
    'renderHomeworks' : ['localize', authorize(CONSTANTS.userRoles.group2Roles)]
  },
  GradesController:{
    // '*': ['sessionAuth', 'localize'],
    'renderRecordGrades' : ['localize', authorize(CONSTANTS.userRoles.group2Roles)],
    'RecordGrades' : ['localize',authorize(CONSTANTS.userRoles.group2Roles)],
    'addStudentGrades' : ['localize', authorize(CONSTANTS.userRoles.group2Roles)],
    'createGrade' : ['localize', authorize(CONSTANTS.userRoles.group2Roles)],
    'updateStudentGrades'  : ['localize', authorize(CONSTANTS.userRoles.group2Roles)]

  },
  AttendanceController: {
    // '*': ['sessionAuth', 'localize'],
    'renderRecordAttend' : ['localize', authorize(CONSTANTS.userRoles.group2Roles)],
    'RecordAttend' : ['localize', authorize(CONSTANTS.userRoles.group2Roles)]
  },
  AnnouncementsController:{
    // '*': ['sessionAuth', 'localize'],
    'renderAnnouncements' : ['sessionAuth', 'localize'],
    'renderAnnouncementsNew' : ['sessionAuth', 'localize', authorize(CONSTANTS.userRoles.group2Roles)]
  },
  AuthenticateController : {
    'forgetPassword' : ['localize'],
    'resetPassword' : ['localize'],
    'login' : ['localize'],
    'logout' : ['localize']
  },
  AuthController : {
    '*' : ['localize'],
    'forgetPassword' : ['localize'],
    'resetPassword' : ['localize'],
    'renderForgetPassword' : ['localize']
  }

  /***************************************************************************
  *                                                                          *
  * Here's an example of mapping some policies to run before a controller    *
  * and its actions                                                          *
  *                                                                          *
  ***************************************************************************/
	// RabbitController: {

		// Apply the `false` policy as the default for all of RabbitController's actions
		// (`false` prevents all access, which ensures that nothing bad happens to our rabbits)
		// '*': false,

		// For the action `nurture`, apply the 'isRabbitMother' policy
		// (this overrides `false` above)
		// nurture	: 'isRabbitMother',

		// Apply the `isNiceToAnimals` AND `hasRabbitFood` policies
		// before letting any users feed our rabbits
		// feed : ['isNiceToAnimals', 'hasRabbitFood']
	// }
};
