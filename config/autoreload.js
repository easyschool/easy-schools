module.exports.autoreload = {
  active: true,
  usePolling: false,
  dirs: [
    "api/models",
    "api/controllers",
    "api/policies",
    "api/services"
  ],
  ignored: [
    "**.ts"
  ]
};
