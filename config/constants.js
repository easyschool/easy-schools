module.exports.constants = {
  "url" : {
    "local" : "http://localhost:1337",
    "development" : "http://easy-school.co",
    "production" : "http://easy-school.co"
  },
  "baseURL":{
    "local" : "http://localhost",
    "development" : "http://easy-school.co",
    "production" : "http://easy-school.co"
  },
  "easySchools" : "Easy Schools",
  "userRoles" : {
    "individuals" : {
      "admin" : "admin",
      "superuser" : "superuser",
      "teacher" : "teacher",
      "student" : "student",
      "parent" : 'parent'
    },
    "group1Roles" : ["admin", "superuser"],
    "group2Roles" : ["admin", "superuser", "teacher"],
    "group3Roles" : ["admin", "superuser", "teacher", "student", "parent"],
    "group4Roles" : ["admin", "superuser","student", "parent"],
    "group5Roles" : ["student", "parent"],
    "allRoles" : ["admin", "superuser", "teacher", "student", "parent"]
  },
  userTypes : {
    'admin' : 'admin',
    'superuser' : 'superuser',
    'teacher' : 'teacher',
    'student' : 'student',
    "parent" : 'parent'
  },
  announce : {
    types : {
      'school' : 'school',
      'master' : 'master',
      'class' : 'class',
      'student' : 'student',
      'parent' : 'parent'
    }
  },
  httpCodes : {
    "request_missing_param" : 422,
    "not_found" : 404
  },
  easySchoolsMail : {
    support : {
      email : 'support@easyschools.org',
      password: 'easy@417',
      service : 'Zoho'
    }
  },
  awsS3Config : {
      bucket: "use-rprofiles",
      access_key: "AKIAID5AOLBO3HODCTNQ",
      secret_key: "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",
      region: "us-east-2",
      acl: "public-read",                                                 // to allow the uploaded file to be publicly accessible
      "x-amz-algorithm": "AWS4-HMAC-SHA256",                              // algorithm used for signing the policy document
      success_action_status: "201"                                        // to return an XML object to the browser detailing the file state
  },
  emailMessageType : {
    parentAccCreated : {
      message : "No reply - Your account has been created",
      name : 'parentAccCreated'
    },
    parentAccAddedChild : {
      message : "Your son has been added to your account",
      name : "parentAccAddedChild"
    },
    accCreated : {
      message : "Your account has been created",
      name : 'accCreated'
    },
    forgetPassword : {
      message : "This email to reset your password",
      name : 'forgetPassword'
    }
  },
  statuses : {
    "created" : "created",
    "new" : "new"
  },
  mail:{
    response : 'response',
    err : 'err'
  },
  "forget_password_link_timeout" : 2880, /*in minutes, two days*/
  'pages' : {
    'schools' : "The page that has all schools",
    'masters home' : "Master home page"
  },
  patchInsert : {
    en : {
      "First_name" : "First name",
      "Full_father_name" : "Full father name",
      "Email" : "Email",
      "Mobile_Number" : "Mobile Number",
      "Birthdate" : "Birthdate",
      "SSN" : "SSN",
      "Homenumber" : "Homenumber",
      "Address" : "Address",
      "Year" : "Year",
      "Month" : "Month",
      "Day" : "Day",
      "Parent_name" : "Parent name",
      "Parent_email" : "Parent email",
      "Parent_mobile_number" : "Parent mobile number",
      "Parent_job" : "Parent Job",
      "UserName" : "Student username",
      "Password" : "Student Password",
      "ParentUserName" : "Parent username",
      "ParentPassword" : "Parent Password"

    },
    "ar" : {
      "First_name" : "الاسم الأول",
      "Full_father_name" : "اسم الأب بالكامل",
      "Email" : "ايميل",
      "Mobile_Number" : "رقم الموبايل",
      "SSN" : "الرقم القومي",
      "Homenumber" : "رقم المنزل",
      "Address" : "العنوان",
      "Year" : "سنة الميلاد",
      "Month" : "شهر الميلاد",
      "Day" : "يوم الميلاد",
      "Parent_name" : "اسم ولي الأمر",
      "Parent_email" : "ايميل ولي الأمر",
      "Parent_mobile_number" : "رقم موبايل ولي الأمر",
      "Parent_job" : "وظيفة ولي الأمر",
      "UserName" : "اسم المستخدم للطالب",
      "Password" : "كلمة المرور للطالب",
      "ParentUserName" : "اسم المستخدم لولي الـأمر",
      "ParentPassword" : "كلمةالمرور لولي الأمر"

    }
  }
}
