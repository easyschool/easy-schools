exports.permissionUtils = {};
exports.permissionUtils.shouldISeeThis = function(userType, roles){

  if(!userType || !roles){
    return false;
  }

  if(roles.indexOf(userType) >= 0){
    return true;
  }

  return false;
}
