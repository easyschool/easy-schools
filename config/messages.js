module.exports.messages = {
  "users" : {
    master : 'Master',
    teacher : 'Teacher',
    student : 'Student',
    parent : 'Parent',

    userCreated : {
      name : 'userCreated',
      message : "Account for :1 is created successfully - password is :2"
    },
    studentCreated :{
      name : "studentCreated",
      message : "Student is created successfully, added to parent account, parent is notified by mail."
    },
    studentCreatdParentMailError :{
      name : "studentCreated",
      message : "Student is created successfully, added to parent account, no mail sent to parent, something wrong with parent mail."
    },
    attachedStudent : {
      name: 'attachedStudent',
      message : 'Student :1 is attached to parent :2'
    },
    mailSentSuccess : {
      name : 'mailSuccess',
      message : 'Mail is sent to :1 successfully'
    },
    mailSentFail : {
      name : 'mailFail',
      message : 'Something not right with email :1, of user :2'
    },
    "forget_password_email_sent" : {
      name : "emailSentForget",
      message : {
        "en" : "An email is sent, please check your inbox",
        "ar" : "تم ارسال ايميل لاعادة ضبط كلمة السر"
      }
    }
  },
  success : {
    update : {
      name : 'update success',
      message : 'User data has been updated successfully'
    }
  },
  fail : {
    update : {
      name : 'fail update',
      message : 'Update Operation has been failed'
    }

  },
  businessValidation : {
    updateMaster : {
      name : 'update master',
      message : "A master can't edit another master"
    }
  },
  updatePassword : {
    error : {
      'name' : 'updatePasswordError',
      'message' : 'Old Password does not match'
    },
    confirmPassword : {
      'name' : 'confirmPassword',
      'message' : 'New password and confirm password don\'t match'
    }
  },
  "email_not_found" : {
    "code" : "email_not_found",
    "en" : "This account is not exist",
    "ar" :"هذا الحساب غير موجود"
  },
  "password_confirm_error" : {
    "code" : "password_confirm_error",
    "message" : {
      "en" : "Password and confim password do not match",
      "ar" : "كلمة السر و تأكيد كلمة السر غير متطابقتين"
    }
  },
  "password_confirm_not_exist" : {
    "code" : "password_confirm_not_exist",
    "message" : {
      "en" : "Password and confim password must be exist",
      "ar" : "يجب ادخال كلمة السر و تأكيد كلمة السر"
    }
  },
  "not_authorized_action" : {
    "code" : "not_authorized_action",
    "message" : {
      "en" : "You are not authorized to do this action",
      "ar" : "انت لست مؤهل لهذا الحدث"
    }
  },
    "invalid_reset_password_token" : {
      "code" : "invalid_reset_password_token",
      "message" : {
        "en" : "Something not right, plesase try to resend email to reset password",
        "ar" : "حدث خطأ من فضلك حاول بارسال ايميل مرة اخري لتعيد ضبط كلمة السر"
      }
    },
    "change_password_success" : {
      "code" : "change_password_success",
      "message" : {
        "en" : "Password changed successfully",
        "ar" : "تم تحديث كلمة السر بنجاح"
      }
    }



}
