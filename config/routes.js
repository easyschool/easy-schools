/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/
  'get /lang/:lang': 'LocaleController.setLocale',
  'get /as3' : 'AWSController.getS3UploadCredentials',
  'get /': 'RootController.renderRoot',
  'get /login': 'AuthenticateController.renderLogin',
  'post /login': 'AuthenticateController.login',
  'get /logout': 'AuthenticateController.logout',
  'post /logout': 'AuthenticateController.logout',
  'get /search': 'UsersController.search',
  'get /dataview': 'UsersController.dataview',
  'put /api/divisions' : 'DivisionController.updateDivison',
  'delete /api/dvisions/:id' : 'DivisionController.delteDivision',
  'get /api/divisions' : 'DivisionController.getDivisions',
  'post /api/login': 'AuthenticateController.apilogin',
  'post /api/user': 'UsersController.apiuserdata',
  'post /api/update': 'UsersController.apiupdate',
  'get /api/users' : 'UsersController.getUsers',
  'put /api/users/students': 'UsersController.updateStudent',
  'post /api/subjects': 'UsersController.apisubjects',
  'post /api/subjects/:subject_id/students' : 'SubjectsController.assignStudentToOptionalSub',
  'put /api/subjects/:subject_id/students' : 'SubjectsController.updateStudentSubjects',
  'get /api/attendances': 'UsersController.apiattendance',
  'post /api/homework': 'UsersController.apihomework',
  'get /api/attendance' : 'AttendanceController.getAttendanceByDate',
  'get /api/attendances' : 'AttendanceController.getAttendances',
  'post /api/school': 'UsersController.apischool',
  'get /api/announcements': 'UsersController.apiannouncement',
  'post /api/announcements': 'UsersController.apiannouncement',
  'post /api/classes': 'UsersController.apiclasses',
  'get /api/classes' : 'ClassesController.getClasses',
  'post /api/gra' : 'GradesController.createGrad',
  'put /api/grades' : 'GradesController.updateGrade',
  'delete /api/grades/:id' : 'GradesController.deleteGrade',
  'post /api/studentgrades' : 'GradesController.addStudentGrades',
  'put /api/studentgrades' : 'GradesController.updateStudentGrades',
  'get /api/studentgrades' : 'GradesController.getStudentSubjectGradesExpanded',
  'get /api/grades' : 'GradesController.getGrades',
  'get /schools': 'SchoolsController.renderSchools',
  'get /schools/new': 'SchoolsController.renderNewSchool',
  'get /schools/remove': 'SchoolsController.renderRemoveSchool',
  'post /schools/:school_id/remove': 'SchoolsController.removeSchool',
  'post /schools/new': 'SchoolsController.createNewSchool',
  'post /schools/remove': 'SchoolsController.removeSchool',

  'get /schools/:school_id': 'SchoolsController.renderSchool',
  'get /schools/:school_id/update': 'SchoolsController.renderUpdateSchool',
  'post /schools/:school_id/update': 'SchoolsController.updateSchool',

  'get /schools/:school_id/:user_id/subjects_list': 'SubjectsController.renderSubjectsList',

  'get /schools/:school_id/years': 'DivisionController.renderDivsion',
  'get /schools/:school_id/years/new': 'DivisionController.renderDivsionNew',
  'post /schools/:school_id/years/new': 'DivisionController.createDivision',
  'get /schools/:school_id/divisions' : 'DivisionController.getAllDivisions',

  'get /schools/:school_id/announce': 'AnnouncementsController.renderAnnouncements',
  'get /schools/:school_id/announces': 'AnnouncementsController.getAnnounces',
  'get /schools/:school_id/announces/mine': 'AnnouncementsController.getMyAnnouncements',
  'get /schools/:school_id/announce/new': 'AnnouncementsController.renderAnnouncementsNew',
  'post /schools/:school_id/announce/new': 'AnnouncementsController.createAnnouncement',
  'get /schools/:school_id/users/:user_id/home'  : 'UsersController.renderUserHomePage',
  'get /schools/:school_id/classes/new': 'ClassesController.renderClassesNew',
  'get /schools/:school_id/classes/remove': 'ClassesController.renderClassesRemove',
  'post /schools/:school_id/classes/new': 'ClassesController.createClass',
  'post /schools/:school_id/classes/:class_id/remove': 'ClassesController.removeClass',
  'post /schools/:school_id/classes/:class_id/update': 'ClassesController.updateClass',
  'get /schools/:school_id/classes/:class_id/schedule' : 'ClassesController.renderClassSchedule',
  'get /schools/:school_id/users': 'UsersController.renderUsers',

  'get /schools/:school_id/users/new': 'UsersController.renderUsersNew',
  'get /users/:user_id/update': 'UsersController.renderUserUpdate',
  'post /users/:user_id/update': 'UsersController.updateUsers',
  'post /users/:user_id/userImage': 'UsersController.userImage',
  'post /users/:user_id/delete': 'UsersController.removeUser',
  'post /schools/:school_id/users/new': 'UsersController.CreateUser',

  'get /schools/:school_id/classes/:class_id': 'ClassesController.renderClass',
  'get /schools/:school_id/classes/:class_id/update': 'ClassesController.renderClassUpdate',

  'get /schools/:school_id/classes/:class_id/subjects': 'SubjectsController.renderSubjects',
  'get /schools/:school_id/classes/:class_id/users/:user_id/subjects': 'SubjectsController.renderStudentSubjects',
  'get /schools/:school_id/classes/:class_id/subjects/new': 'SubjectsController.renderSubjectsNew',
  'post /schools/:school_id/classes/:class_id/subjects/new': 'SubjectsController.createSubject',

  'get /schools/:school_id/classes/:class_id/subjects/:subject_id': 'SubjectsController.renderSubject',
  'get /schools/:school_id/classes/:class_id/subjects/:subject_id/update': 'SubjectsController.renderSubjectUpdate',
  'patch /schools/:school_id/classes/:class_id/subjects/:subject_id/update': 'SubjectsController.updateSubject',
  'get /schools/:school_id/classes/:class_id/subjects/:subject_id/delete': 'SubjectsController.deleteSubject',

  'get /schools/:school_id/classes/:class_id/subjects/:subject_id/hw': 'HomeworkController.renderHomeworks',
  'post /schools/:school_id/classes/:class_id/subjects/:subject_id/hw/new': 'HomeworkController.createHomework',
  'post /schools/:school_id/classes/:class_id/subjects/:subject_id/hw/': 'HomeworkController.updateHomework',
  'post /schools/:school_id/classes/:class_id/subjects/:subject_id/users/:user_id/hw/answer' : 'HomeworkController.submitAnswer',
  'delete /api/homeworks/:hwId': 'HomeworkController.deleteHomework',
  'get /schools/:school_id/classes/:class_id/subjects/:subject_id/attend/new': 'AttendanceController.renderRecordAttend',
  'post /schools/:school_id/classes/:class_id/subjects/:subject_id/attend/new': 'AttendanceController.RecordAttend',
  'get /schools/:school_id/classes/:class_id/subjects/:subject_id/grades/new': 'GradesController.viewGrades',
  'post /schools/:school_id/classes/:class_id/subjects/:subject_id/grades/new': 'GradesController.RecordGrades',
  // 'get /schools/:school_id/classes/:class_id/subjects/:subject_id/grades/view': 'GradesController.viewGrades',
  'get /schools/:school_id/classes/:class_id/subjects/:subject_id/attend/view': 'AttendanceController.viewattendance',
  "put /api/attendances/:id" : 'AttendanceController.updateAttendance',


  'get /schools/:school_id/classes/:class_id/subjects/:subject_id/users': 'UsersController.renderUsersSubject',

  'get /schools/:school_id/classes/:class_id/subjects/:subject_id/users/:user_id/menu': 'UsersController.renderMenu',

  'get /schools/:school_id/classes/:class_id/subjects/:subject_id/users/:user_id/grades': 'GradesController.renderGrades',
  'get /schools/:school_id/classes/:class_id/subjects/:subject_id/users/:user_id/attend': 'AttendanceController.renderAttendance',
  'get /users/:user_id/attendances' : "AttendanceController.getStudentAttendance",
  'get /schools/:school_id/classes/:class_id/subjects/:subject_id/users/:user_id/hw': 'HomeworkController.renderStudentHomework',
  'post /auth/forget-password' : 'AuthController.forgetPassword',
  'post /auth/reset-password' : 'AuthController.resetPassword',
  'get /auth/forget-password' : 'AuthController.renderForgetPassword',
  'get /auth/reset-password' : {view: 'password/reset-password'},
  'get /users' : "UsersController.getUsers",
  'get /api/userProfile' : "UsersController.renderMobProfileImage",
  'post /api/attachFile' : "AttachmentController.attachFile",
  'post /logs' : "ReporterController.logReportData",
  'put /logs/:id' : "ReporterController.updateReportData",
  'get /logs' : 'ReporterController.genereateCSVReport',
  'post /schools/:school_id/users/patch' : 'UsersController.patchInsertUsers',
  'get  /schools/:school_id/users/patch' : 'UsersController.renderPatchInsert',
  'get /getdatabase':'getdatabase.getdb'
  //admin
  /*  Full Route
  *   /schools/:school_id/classes/:class_id/subjects/:subject_id/users/:user_id/r'grades|attend|hw'
  *

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
