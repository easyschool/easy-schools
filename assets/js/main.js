function disableMe(me){
  setTimeout(()=>{
    me.setAttribute('disabled','true');
  },1);
  setTimeout(()=>{
    me.removeAttribute('disabled');
  },1000);

  return true;
}
function showLoader(){
  $('#loader').show();
}


function hideLoader(){
  $('#loader').hide();
}

function changeStateValue(checkbox, selectorClass){
  $('.' + selectorClass).prop('checked', checkbox.checked);

}
