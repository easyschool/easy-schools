
var selectValues = new Array();
  var checkedvalues = new Array();

    $('#students_update').click(function(){
                $('#studentsssection').attr('hidden' , false);

          $.ajax({
            type:'GET',
            data:{ids:checkedvalues},
            url: "/api/classes",
            success: function(result){
                $('#students-multiple-selected').html('');
               for (var i = 0 ; i < result.length ; i++) {

                   $('#students-multiple-selected').append($(' <optgroup>', {
                          label: result[i].name,
                          class: result[i].id
                      }));

                   for (var j = 0 ; j < result[i].students.length ; j++) {

                           $('#students-multiple-selected').append($('<option>', {
                            value: result[i].students[j].id,
                            text: result[i].students[j].firstName + "  " + result[i].students[j].lastName
                      }));

                   }

             $('#students-multiple-selected').multiselect('rebuild');
           }

            }


          });

    });
    $('#parents_update').click(function(){
          $('#parentsssection').attr('hidden' , false);

          $.ajax({
            type:'GET',
            data:{ids:checkedvalues},
            url: "/api/classes",
            success: function(result){
                $('#parents-multiple-selected').html('');
               for (var i = 0 ; i < result.length ; i++) {

                   $('#parents-multiple-selected').append($(' <optgroup>', {
                          label: result[i].name,
                          class: result[i].id
                      }));

                   for (var j = 0 ; j < result[i].students.length ; j++) {

                           $('#parents-multiple-selected').append($('<option>', {
                            value: result[i].students[j].parent,
                            text: result[i].students[j].firstName + "  " + result[i].students[j].lastName
                      }));

                   }

             $('#parents-multiple-selected').multiselect('rebuild');
           }

            }


          });

    });


  $('#classes-multiple-selected').multiselect({
      buttonWidth: '100%',
      enableClickableOptGroups: true,
      enableCollapsibleOptGroups: true,
      enableFiltering: true,
       onChange: function(option, checked) {
              checkedvalues.length=0;
              selectValues.length=0;
              $('#classes-multiple-selected').each(function() {
                  selectValues.push($(this).val());

              });

              for(var x = 0 ; x<selectValues[0].length;x++){
                checkedvalues.push(selectValues[0][x]);
              }
          }

   });

    $('.select_multiple').multiselect({

      includeSelectAllOption: true,
      enableClickableOptGroups: true,
      enableCollapsibleOptGroups: true,
      enableFiltering: true,
      buttonWidth: '100%',



     });

    function getdivision(schoolid){
        renderdivision();
          $.ajax({
            url: "/api/divisions?schoolId="+schoolid,
            success: function(result){
               $('#divisions-multiple-selected').html('');


              for (var i = 0 ; i < result.length ; i++) {
                 $('#divisions-multiple-selected').append($('<option>', {
                          value: result[i].id,
                          text: result[i].name
                      }));
            }

              $('#divisions-multiple-selected').multiselect('rebuild');

          }});

    };


      function getclasses(schoolid, userId, type){
         renderclasses();
         var url;
         if(type != 'superuser'){
           url = '/api/classes?userId=' + userId + '&type=' + type;
         }else{
           url =  "/api/divisions?schoolId=" + schoolid;
         }
          $.ajax({
            url: url,
            success: function(result){
               $('#classes-multiple-selected').html('');
                for (var i = 0 ; i < result.length ; i++) {
                  if(result[i].classes.length!=0){
                   $('#classes-multiple-selected').append($(' <optgroup>', {
                          label: result[i].name,
                          class: result[i].id
                      }));

                   for (var j = 0 ; j < result[i].classes.length ; j++) {


                           $('#classes-multiple-selected').append($('<option>', {
                           value: result[i].classes[j].id,
                            text: result[i].classes[j].name
                      }));

                   }
                }
             $('#classes-multiple-selected').multiselect('rebuild');
           }
          }});

    };

        function getteacherandmasters(schoolid){
        renderteachers();
          $.ajax({
            url:"/api/users?schoolId="+schoolid+"&type=teacher&type=admin",
            success: function(result){
               $('#teachers-multiple-selected').html('');
               for (var i = 0 ; i < result.length ; i++) {

                    if(result[i].type=='admin'){
                           $('#teachers-multiple-selected').append($('<option>', {
                           value: result[i].id,
                            text: result[i].firstName +"   " +result[i].lastName +" > Master",
                    }));
                  }else{
                            $('#teachers-multiple-selected').append($('<option>', {
                             value: result[i].id,
                            text: result[i].firstName +"   " +result[i].lastName +" > Teacher",
                    }));


                  }
                }


                $('#teachers-multiple-selected').multiselect('rebuild');
          }});

    };

    function getparents(schoolid, userId, type){
      getclasses(schoolid, userId, type);
      renderparents();
    };


    function getstudents(schoolid, userId, type){

      getclasses(schoolid, userId, type);
      renderstudents();
    };

      function renderschool(){
          $('.sections').attr('hidden' , true);
          $('#classes-multiple-selected').multiselect('deselectAll', true);
          $('#divisions-multiple-selected').multiselect('deselectAll', true);
          $('#teachers-multiple-selected').multiselect('deselectAll', true);
          $('#students-multiple-selected').multiselect('deselectAll', true);
          $('#parents-multiple-selected').multiselect('deselectAll', true);

      };
      function renderdivision(){
          $('.sections').attr('hidden' , true);
          $('#divisionssection').attr('hidden' , false);
          $('#classes-multiple-selected').multiselect('deselectAll', true);
          $('#teachers-multiple-selected').multiselect('deselectAll', true);
          $('#students-multiple-selected').multiselect('deselectAll', true);
          $('#parents-multiple-selected').multiselect('deselectAll', true);

      };
      function renderclasses(){
          $('.sections').attr('hidden' , true);
          $('#classessection').attr('hidden' , false);
          $('#divisions-multiple-selected').multiselect('deselectAll', true);
          $('#teachers-multiple-selected').multiselect('deselectAll', true);
          $('#students-multiple-selected').multiselect('deselectAll', true);
          $('#parents-multiple-selected').multiselect('deselectAll', true);

      };
      function renderteachers(){
          $('.sections').attr('hidden' , true);
          $('#teachersssection').attr('hidden' , false);
          $('#divisions-multiple-selected').multiselect('deselectAll', true);
          $('#students-multiple-selected').multiselect('deselectAll', true);
          $('#classes-multiple-selected').multiselect('deselectAll', true);
           $('#parents-multiple-selected').multiselect('deselectAll', true);

      };
      function renderstudents(){
          $('.sections').attr('hidden' , true);
          $('#studentsssection').attr('hidden' , false);
          $('#classessection').attr('hidden' , false);
          $('#divisions-multiple-selected').multiselect('deselectAll', true);
          $('#teachers-multiple-selected').multiselect('deselectAll', true);
          $('#parents-multiple-selected').multiselect('deselectAll', true);


      };

      function renderparents(){
          $('.sections').attr('hidden' , true);
          $('#parentssection').attr('hidden' , false);
          $('#classessection').attr('hidden' , false);
          $('#divisions-multiple-selected').multiselect('deselectAll', true);
          $('#teachers-multiple-selected').multiselect('deselectAll', true);
          $('#students-multiple-selected').multiselect('deselectAll', true);


      };
