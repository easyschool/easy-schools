var Promise = require('promise');

module.exports = {
  /**
  get divisons by school id, or get all divisions
  */
  getAllDivisions : function(schoolId){
    return new Promise((resolve, reject)=>{
      let query = {};
      if(schoolId){
        query.school = schoolId;
      }
      Divisions.find(query).populate('classes').exec((err, expandedDivisons)=>{
        if(!err){
          resolve(expandedDivisons);
        }else{
          reject(err);
        }
      });
    });
  },
  getSchoolDivsionsDeepNested : function(schoolId){
  return new Promise((resolve, reject)=>{
    Divisions.find({
            belongingTo: schoolId
        }).populate('classes')
        .exec(function(err, divisions2) {

          var getClassesPromises = [];
          for(var iDivision = 0; iDivision < divisions2.length; iDivision++){
            var classesIds = [];
            for(var iClass = 0; divisions2 && iClass < divisions2[iDivision].classes.length; iClass++){
              classesIds.push(divisions2[iDivision].classes[iClass].id);
            }
            getClassesPromises.push(ClassService.getClassExpandedByDivision(divisions2[iDivision],classesIds ));
          }
          Promise.all(getClassesPromises).then((divisionsExpanded)=>{
            resolve(divisionsExpanded);
          },(err)=>{
            reject(err);
          });
        });
  });

  }
};
