const Promise = require('promise');
module.exports = {
  getClassById : function(classId){
    return new Promise((resolve, reject)=>{
      Classes.findOneById(classId).populate('students').exec((err, obj)=>{
        if(!err){
          resolve(obj);
        }else{
          reject(err);
        }
      });
    });
  },
  getClasses : function(ids){
    return new Promise((resolve, reject)=>{
      Classes.find({id : ids}).populate('students').exec((err, classes)=>{
        if(!err){
          resolve(classes);
        }else{
          reject(err);
        }
      });
    });
  },
  getClassExpandedByDivision : function(division, classIds){
    return new Promise((resolve, reject)=>{
      Classes.find({id : classIds}).populate('subjects').exec((err, classes)=>{
        if(!err){
          division = division.toJSON();
          division.classes = classes;
          resolve(division);
        }else{
          reject(err);
        }
      });
    });
  },
  getAdminClasses : function(classes, adminId){
    return new Promise((resolve, reject)=>{
      Classes.find().where({or:[{id : classes}, {creator: adminId}]}).populate('division_key').exec((err, objects)=>{
        if(err){
          return reject(err);
        }
        Divisions.find({creator : adminId}).populate('classes').exec((err, masterDivisions)=>{
          if(err){
            return Utils.sendError(res, err);
          }

          var divisions = {};
          let divisionTemp;
          for(let i = 0; i < objects.length; i++){
            divisionTemp = divisions[objects[i].division_key.id];
            if(!divisionTemp ){
              divisions[objects[i].division_key.id] = objects[i].division_key.toJSON();
              divisions[objects[i].division_key.id].classes = [];
            }
            divisions[objects[i].division_key.id].classes.push(objects[i]);
          }
          for(let i = 0; i < masterDivisions.length; i++){
            divisionTemp = divisions[masterDivisions[i].id];
            if(!divisionTemp ){
              divisions[masterDivisions[i].id] = masterDivisions[i].toJSON();
            }else{
              divisions[masterDivisions[i].id].classes = masterDivisions[i].toJSON().classes;
            }
          }

          let divisionsList = [];
          for(let key in divisions){
            divisionsList.push(divisions[key]);
          }
          resolve(divisionsList);
        });
      });
    });

  }


}
