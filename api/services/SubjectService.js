const Promise = require('promise');
module.exports = {
  getSubjectById : function(subjectId){
    return new Promise((resolve, reject)=>{
      Subjects.findOneById(subjectId).populate('students').exec((err, subject)=>{
        if(!err){
          resolve(subject);
        }else{
          reject(err);
        }
      });
    });
  },

  addOptionalStdudentSubjects : function(studentSubjects){
    return new Promise((resolve, reject)=>{

      Studentsubjects.create(studentSubjects).exec((err, newStudentSubjects)=>{
          if(!err){
            return resolve(newStudentSubjects);
          }
          reject(err);
        });


      // let promises = [];
      // for (var i = 0; i < studentSubjects.length; i++) {
      //   studentSubjects[addOptionalStdudentSubject(studentSubjects)];
      // }
      // Promise.all(promises).then((newDocs)=>{
      //   resolve(newDocs);
      // }, (err)=>{
      //   reject(err);
      // });
    });
  },

  updateStudentSubjects : function(studentSubjects){
    return new Promise((resolve, reject)=>{
      let promises = [];
      for (var i = 0; i < studentSubjects.length; i++) {
        promises.push(updateStudentSubject(studentSubjects[i]));
      }

      Promise.all(promises).then((studentSubjects)=>{
        resolve(studentSubjects);
      }, (err)=>{
        reject(err);
      });
    });
  },

  removeStudentSubjects : function(studentSubjects){
    return new Promise((resolve, reject)=>{
      let ids = [];
      for (var i = 0; i < studentSubjects.length; i++) {
        ids.push(studentSubjects[i].id);
      }
      Studentsubjects.destroy({id : ids}).exec((err, result)=>{
        if(!err){
          return resolve(result);
        }

        reject(err);

      });

    });
  }

}

function addOptionalStdudentSubject (optionalStudentSubject){
  return new Promise((resolve, reject)=>{
    Studentsubjects.create(optionalStudentSubject).exec((err, newStudentSubject)=>{
        if(!err){
          return resolve(newStudentSubject);
        }
        reject(err);
      });
  });
}


function updateStudentSubject(studentSubject){
  return new Promise((resolve, reject)=>{
    Grades.update({id : studentSubject.id}, studentSubject).exec((err, studentSubject)=>{
      if(!err){
        return resolve(studentSubject);
      }
      reject(err);
    });
  });
};

function removeStudentSubject(studentSubject){
  return new Promise((resolve, reject)=>{
    Grades.destroy(studentSubject.id).exec((err, studentSubject)=>{
      if(!err){
        return resolve(studentSubject);
      }
      reject(err);
    });
  });
};
