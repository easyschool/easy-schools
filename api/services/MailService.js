const Promise = require('promise');
const CONSTANTS = sails.config.constants;
const EJS = require('ejs');
const nodemailer = require('nodemailer');

let supportTransporter = nodemailer.createTransport({
     service: CONSTANTS.easySchoolsMail.support.service, // no need to set host or port etc.
     auth: {
         user: CONSTANTS.easySchoolsMail.support.email,
         pass: CONSTANTS.easySchoolsMail.support.password
     }
});


module.exports = {
  sendEmail : function(messageType,toEmail, sentInfo){
    return new Promise((resolve, reject)=>{
      if(sentInfo.schoolId){
        Schools.findById(sentInfo.schoolId).exec((err, schools)=>{
          if(err){
            return reject(err);
          }
          sentInfo.school = schools.length > 0 ? schools[0] : {};
          getTemplate(messageType,toEmail, sentInfo, resolve);
        });
      }else{
        getTemplate(messageType,toEmail, sentInfo, resolve);
      }
    });
  }

};

function getTemplate(messageType,toEmail, sentInfo, resolve){
  let path;

  switch (messageType) {
    case CONSTANTS.emailMessageType.parentAccCreated.name:
      path =   __dirname + '/../../views/mailTemplates/parent-created.ejs';
      send(toEmail,CONSTANTS.emailMessageType.parentAccCreated.name, path, sentInfo, resolve);
      break;
    case CONSTANTS.emailMessageType.parentAccAddedChild.name :
      path =   __dirname + '/../../views/mailTemplates/parent-added-child.ejs';
      send(toEmail,CONSTANTS.emailMessageType.parentAccAddedChild.name, path, sentInfo, resolve);
      break;
    case CONSTANTS.emailMessageType.accCreated.name :
      path =   __dirname + '/../../views/mailTemplates/acc-created.ejs';
      send(toEmail,CONSTANTS.emailMessageType.accCreated.name, path, sentInfo, resolve);
      break;
    case CONSTANTS.emailMessageType.forgetPassword.name :
      path =   __dirname + '/../../views/mailTemplates/forgetPassword.ejs';
      send(toEmail,CONSTANTS.emailMessageType.forgetPassword.name, path, sentInfo, resolve);
      break;
    default:
  }

}

function send(email, subject, path, sentInfo, resolve){


  EJS.renderFile(path, sentInfo, {}, function(err, str){
  let mailOptions = {
          from: '"' + CONSTANTS.easySchools + '" <' + CONSTANTS.easySchoolsMail.support.email + '>', // sender address
          to: email, // list of receivers
          subject: CONSTANTS.emailMessageType[subject].message, // Subject line
          html: str // html body
      };
      console.log("email body>>>>>>>>>");
      console.log(str);
    supportTransporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        }
        console.log("Mail is sent successfully");
    });
    //Did that because no way to know if the email is sent successfully.
    resolve("In Progress sending");
  });

}
