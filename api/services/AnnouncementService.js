var Promise = require('promise');
const CONSTANTS = sails.config.constants;
module.exports = {
  createAnnouncement : createAnnouncement,
   getAnnouncementsDateRange : getAnnouncementsDateRange,
   getAssociatedAnnounces : getAssociatedAnnounces
}


 function getAnnouncementsDateRange(startDate, endDate, schoolId){
   return new Promise((resolve, reject)=>{
   Announcements.find({

       start : {'>=' : startDate},
       end : {'<' : endDate},
    school : schoolId
  })
  .populate('classes')
  .populate('users')
  .populate('owner')
  .exec((err, announces)=>{
     if(!err){
       resolve(announces);
     }else{

       reject(err);
     }
    });
  });
}

function getAssociatedAnnounces(userObj, startDate, endDate){

  return new Promise((resolve, reject)=>{
    let promises;
    switch (userObj.type) {
      case CONSTANTS.userTypes.student:
          promises = [
          getSchoolAnnounces(userObj.belongingTo, startDate, endDate),
          getUserAnnounces([userObj.id], startDate, endDate),
          getClassAnnounces([userObj.enrolledClass], startDate, endDate)];
        executeGetAnnouncePromises(promises).then((announces)=>{
          announces = UtilService.removeRedundantFromArrayOfJsons(announces, 'id');
          resolve(announces);
        },(err)=>{
          reject(err);
        });
        break;
      case CONSTANTS.userTypes.admin:
        Users.findOne(userObj.id).populate('masterClasses').exec((err, user)=>{
          if(!err){
            let classIds = [];
            for(let i = 0; i < user.masterClasses.length; i++){
              classIds.push(user.masterClasses[i].id);
            }
            Users.find({'enrolledClass' : classIds, 'type' : CONSTANTS.userTypes.student}).populate('parent').exec((err, students)=>{
              // let userIds = UtilService.getArrayValuesFromJsons(students, 'id');
              let userIds = [];
              if(students && students.length > 0){
                for(let i = 0; i < students.length; i++){
                  userIds.push(students[i]['id']);
                  userIds.push(students[i].parent.id);
                }
              }


                promises = [
                getSchoolAnnounces(userObj.belongingTo, startDate, endDate),
                getUserAnnounces([userObj.id], startDate, endDate),
                getClassAnnounces(classIds, startDate, endDate),
                getUserAnnounces(userIds, startDate, endDate),
                getAnnouncesByOwner(userObj.id)
              ];
              executeGetAnnouncePromises(promises).then((announces)=>{
                announces = UtilService.removeRedundantFromArrayOfJsons(announces, 'id');
                resolve(announces);
              },(err)=>{
                reject(err);
              });
            });

          }
        });

        break;
      case CONSTANTS.userTypes.teacher:
            Subjects.find({id : userObj.subjects}).populate('class').exec((err, subjects)=>{
              if(!err){
                let classIds = [];
                for(let i = 0; i < subjects.length; i++){
                  classIds.push(subjects[i].class.id);
                }

                  promises = [
                  getSchoolAnnounces(userObj.belongingTo, startDate, endDate),
                  getUserAnnounces([userObj.id], startDate, endDate),
                  getClassAnnounces(classIds, startDate, endDate),
                  getAnnouncesByOwner(userObj.id)
                ];
                executeGetAnnouncePromises(promises).then((announces)=>{
                  announces = UtilService.removeRedundantFromArrayOfJsons(announces, 'id');
                  resolve(announces);
                },(err)=>{
                  reject(err);
                });
              }else{
                reject(err);
              }

            });
        break;
      case CONSTANTS.userTypes.parent:
        Users.findOneById(userObj.id).populate('children').exec((err, parent)=>{
          promises = [getUserAnnounces([userObj.id], startDate, endDate)];
          let classIds = [];
          let schoolIds = [];
          for(let i = 0; i < parent.children.length; i++){
            classIds.push(parent.children[i].enrolledClass);
            promises.push(getSchoolAnnounces(parent.children[i].belongingTo, startDate, endDate));
          }
          promises.push(getClassAnnounces(classIds, startDate, endDate));
          executeGetAnnouncePromises(promises).then((announces)=>{
            announces = UtilService.removeRedundantFromArrayOfJsons(announces, 'id');
            resolve(announces);
          },(err)=>{
            reject(err);
          });
        });


        break;
      default:
    }

  });

}
function executeGetAnnouncePromises(promises){
  return new Promise((resolve, reject)=>{
    Promise.all(promises).then((announcesList)=>{
      announces = UtilService.mergeArraysInArray(announcesList)
      resolve(announces);
    },(err)=>{
      reject(err);
    });
  });
}
function getUserAnnounces(userIds, startDate, endDate){
  return new Promise((resolve, reject)=>{
    gettingRelatedAnnounces(AnnounceUser, 'announceuser', 'user', userIds, startDate, endDate).then((announces)=>{
      console.log("user announces");
      console.log(announces);
      resolve(announces);
    }, (err)=>{
      reject(err);
    });

  });
}

function getSchoolAnnounces(schoolId, startDate, endDate){


  return new Promise((resolve, reject)=>{

    let query = {
      school : schoolId,
      'announce_type' : CONSTANTS.announce.types.school,
       start : {'>=' : startDate},
       end : {'<=' : endDate},
    };
    getAnnounces(query).then((announces)=>{
      console.log("school announces");
      console.log(announces);
      resolve(announces);
    },(err)=>{
      reject(err);
    });
  });
}

function getClassAnnounces(classIds, startDate, endDate){

    return new Promise((resolve, reject)=>{
      gettingRelatedAnnounces(AnnounceClass, 'announceclass', 'class', classIds, startDate, endDate).then((announces)=>{
        console.log("class announces");
        console.log(announces);
        resolve(announces);
      }, (err)=>{
        reject(err);
      });

    });

}

function getAnnounces(query){
  return new Promise((resolve, reject)=>{
    Announcements.find(query)
    .populate('classes')
    .populate('users')
    .populate('owner')
    .populate('school').exec((err, announces)=>{
      if(!err){
        return resolve(announces);
      }
      reject(err);
    });
  });
}



function  createAnnouncement(announceObj){
  return new Promise((resolve, reject)=>{

     Announcements.create(announceObj).exec(function createCB(err, createAnnounce) {

       if(!err){
         resolve(createAnnounce);
       }else{
         console.log(err);
         reject(err);
       }
     });
   });

 }

function getAnnouncesByOwner(id){
  return new Promise((resolve, reject)=>{
    Announcements.find({owner : id}).populate('owner').exec((err, announces)=>{
      if(!err){
        resolve(announces);
      }else{
        reject(err)
      }
    });
  });
}

function gettingRelatedAnnounces(model, tableName, relatedFieldName, relatedIds, startDate, endDate){
  return new Promise((resolve, reject)=>{
    if(relatedIds && Array.isArray(relatedIds) && relatedIds.length == 0){
      return resolve([]);
    }
    let query = "select announce from " + tableName + " where " + relatedFieldName + " in " + UtilService.makeArrayWithWithGroupBrackets(relatedIds);
    model.query(query, null, (err, results)=>{
      if(!err){
        let ids = [];
        for(let i = 0; i < results.length; i++){
          ids.push(results[i].announce);
        }
        let query = {
          id : ids,
         start : {'>=' : startDate},
         end : {'<=' : endDate},
        }
        getAnnounces(query).then((announces)=>{
          resolve(announces);
        },(err)=>{
          reject(err);
        });
      }
    });
  });
}
