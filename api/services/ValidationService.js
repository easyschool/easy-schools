const Promise = require('promise');
const CONSTANTS = sails.config.constants;
const MESSAGES = sails.config.messages;
module.exports = {
  validateEditUser : validateEditUser
};

function validateEditUser(req, userObj){
  if(userObj.id !== req.user.id  &&req.user.type === CONSTANTS.userTypes.admin && userObj.type === CONSTANTS.userTypes.admin){
    return MESSAGES.businessValidation.updateMaster.name;
  }
  return null;
}
