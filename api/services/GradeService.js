const Promise = require('promise');

exports.createGrade = function(gradeObj){
  return new Promise((resolve, reject)=>{
    Grades.create(gradeObj).exec((err, newGrade)=>{
      if(!err){
        return resolve(newGrade);
      }
      reject(err);
    });
  });
};

exports.updateGrade = function(gradeObj){
  return new Promise((resolve, reject)=>{
    Grades.update({id : gradeObj.id}, gradeObj).exec((err, updatedGrade)=>{
      if(!err){
        return resolve(updatedGrade);
      }
      reject(err);
    });
  });
};

exports.addStudentGrade = function(studentGrade){

  return new Promise((resolve, reject)=>{
  UserGrade.create(studentGrade).exec((err, newStudentGrade)=>{
      if(!err){
        return resolve(newStudentGrade);
      }
      reject(err);
    });
  });
}


exports.updateStudentGrade = function(studentGrade){

  return new Promise((resolve, reject)=>{
  UserGrade.update(studentGrade.id, studentGrade).exec((err, updatedStudentGrade)=>{
      if(!err){
        return resolve(updatedStudentGrade?updatedStudentGrade[0]:null);
      }
      reject(err);
    });
  });
}

function getSubjectGrades(subjectId){
  return new Promise((resolve, reject)=>{
    Grades.find({subject : subjectId}).exec((err, grades)=>{
      if(!err){
        return resolve(grades);
      }
      reject(err);
    });
  });
}

function getStudentGrades(query){
  return new Promise((resolve, reject)=>{
    UserGrade.find(query).populate('grade').exec((err, studentGrades)=>{
        if(!err){
          return resolve(studentGrades);
        }
        reject(err);
      });
  });
}

function getStudentSubjectGradesExpanded(studentId, subjectId){
  return new Promise((resolve, reject)=>{
    getSubjectGrades(subjectId).then((grades)=>{
      const gradeIds = UtilService.getArrayValuesFromJsons(grades, 'id');
      return getStudentGrades({student : studentId, grade : gradeIds});
    }, (err)=>{
      reject(err);
    }).then((studentGrades)=>{
      Users.findById(studentId).exec((err, students)=>{
        if(!err){
          let studentJson = students[0].toJSON();
          studentJson.studentGrades = studentGrades;
          resolve(studentJson);
        }
      });
    },(err)=>{
      reject(err);
    });
  });
}

function getClassSubjectGradesExpanded(classId, subjectId){
  return new Promise((resolve, reject)=>{

    Users.find({enrolledClass : classId}).exec((err, students)=>{
      Subjects.findById(subjectId).populate('students').exec((err, subjects)=>{
        let promises = [];
        if(!err && subjects[0].optional){

          for(let i = 0; i < subjects[0].students.length; i++){
            promises.push(getStudentSubjectGradesExpanded(subjects[0].students[i].id, subjectId));
          }

        }else if(!err){
          for(let i = 0; i < students.length; i++){
            promises.push(getStudentSubjectGradesExpanded(students[i].id, subjectId));
          }
        }

        Promise.all(promises).then((studentGradesList)=>{
          if(!err){
            return resolve(studentGradesList);
          }
        }, (err)=>{
          reject(err);
        });

        
      });


    });
  });
}

exports.getSubjectGrades = getSubjectGrades;
exports.getStudentSubjectGradesExpanded = getStudentSubjectGradesExpanded;
exports.getClassSubjectGradesExpanded = getClassSubjectGradesExpanded;
