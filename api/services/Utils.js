module.exports = {
  redirectToRoot : function(res){
    res.redirect("/");
  },
  sendError : function(res, err){
    return res.serverError(err);
  }
}
