const jwt = require('jsonwebtoken');
const Promise = require('promise');
const crypto = require('crypto');
const CONSTANTS = sails.config.constants;
const sessionConstant = sails.config.session;

exports.verifyToken = function(token){
  return new Promise((resolve, reject)=>{
    jwt.verify(token, sessionConstant.secret, function (err, decoded) {
      if (!err) {
        return resolve(decoded);
      }
      return reject(err);
    });
  });
};

exports.generateJwt = function(user) {
  var expiry = new Date();
  let signObj = user;

  //sessionTimeOut is in minutes
  const sessionTimeOut = parseInt(process.env.SESSION_TIMEOUT);
  signObj.exp = parseInt(expiry.getTime() / 1000) + 60 * sessionTimeOut;
  let token = jwt.sign(signObj, sessionConstant.secret);
  return token;
};

exports.generateJWTBasedOnTime = function(user, minutes){
  var expiry = new Date();
  let signObj = user;

  //sessionTimeOut is in minutes
  const timeout = parseInt(minutes);
  signObj.exp = parseInt(expiry.getTime() / 1000) + 60 * timeout;
  let token = jwt.sign(signObj, sessionConstant.secret);
  return token;
}

exports.setPassword = function(object, password){
  object.salt = crypto.randomBytes(16).toString('hex');
  object.hash = crypto.pbkdf2Sync(password, object.salt, 1000, 64, 'sha512').toString('hex');
  return object;
};
