const Promise = require('promise');
const bcrypt = require('bcrypt');
const json2csv = require('json2csv');
const CONSTANTS = sails.config.constants;
module.exports = {
  'getOrCreateParent' : getOrCreateParent,
  'addStudent' : addStudent,
  'getUsers' : getUsers,
  'createUser' : createUser,
  'createMaster' : createMaster,
  'updateStudent' : updateStudent,
  'compareOldPassword' : compareOldPassword,
  'insertPatchStudents' : insertPatchStudents
};


function createMaster(master, masteredClasses){
  return new Promise((resolve, reject)=>{
    createUser(master).then((result)=>{
      Users.findOne(result.user.id).exec((err, object)=>{
        let classesIds = [];
        for(let i = 0; i < masteredClasses.length; i++){
            classesIds.push(parseInt(masteredClasses[i]));
        }
        object.masterClasses.add(classesIds);
        //Keep the origin password
        const password = object.password;
        delete object.password;
        object.save();
        object.password = result.user.password;
        const mailType = CONSTANTS.emailMessageType.accCreated.name;

        MailService.sendEmail(mailType, object.email, {'user' : object, schoolId : result.user.belongingTo}).then((result)=>{
          resolve({'result' : result, 'user' : object});
        }, (err)=>{
          reject(err);
        });
        resolve(result);


      });
    }, (err)=>{
      reject(err);
    });
  });
}

function createUser(user){
  return new Promise((resolve, reject)=>{
    const password = PasswordService.getRandomPassword();
    user.password = password;
    Users.create(user)
    .exec((err, createdUser)=> {
      if(!err) {
        //set origin password
        createdUser.password = password;
        resolve({status : "success", user : createdUser});
      }else{
        reject(err);
      }
    });

  });
}

function getUsers(query){
  return new Promise((resolve, reject)=>{
      Users.find(query).exec((err, users)=>{
      if(!err){
        resolve(users);
      }else{
        reject(err);
      }
    });
  });
}

function updateStudent(parentCriteria, student){
    return new Promise((resolve, reject)=>{

      getOrCreateParent(parentCriteria).then((parentInfo)=>{
        let parent = parentInfo.object;
        student.parent = parent.id;
        Users.update(student.id, student).exec(function createCB(err, updated) {
          if (err) reject(err);

          resolve(updated[0]);

        });
      }, (err)=>{
        reject(err);
      });
    });
}

function addStudent(parentCriteria,user){
  return new Promise((resolve, reject)=>{
    getOrCreateParent(parentCriteria).then((parentInfo)=>{
      let parent = parentInfo.object;

      user.parent = parent.id;

      //set password for student
      user.password = PasswordService.getRandomPassword();

      Users.create(user).exec((function(parentResults,student){
          return function(err, newStudent){
            if(!err){
              const parentMailType = CONSTANTS.emailMessageType.parentAccAddedChild.name;

              const studentMailType = CONSTANTS.emailMessageType.accCreated.name;

              Promise.all([sendParentMailPromise( parentMailType, parentResults, student, user), sendStudentMailPromise(studentMailType, student )]).then((results)=>{
                resolve({parent : parentResults, student : student, response: results});
              },(err)=>{
                resolve(err);
              });

            }else{
              reject(err);
            }
          }
        }
      )(parentInfo, user));
    },(err)=>{
      console.log(err);
      Utils.sendError(err);
    });
  });
}


const sendParentMailPromise = function(mailType, parentResults, student){
    return new Promise((resolve, reject)=>{


    MailService.sendEmail(mailType, parentResults.object.email, {'user' : parentResults.object, 'child' : student, schoolId : student? student.belongingTo : parentResults.object.belongingTo}).then((result)=>{
      let returnData = {};
      returnData[CONSTANTS.mail.response] = result;
      resolve(returnData);
    },(err)=>{
    let returnData = {};
    returnData[CONSTANTS.mail.err] = err;
    resolve(returnData);
  });



  });
};

const sendStudentMailPromise = function(mailType, student){
    return new Promise((resolve, reject)=>{

      MailService.sendEmail(mailType, student.email, { 'user' : student, schoolId: student.belongingTo}).then((result)=>{
        let returnData = {};

        returnData[CONSTANTS.mail.response] = result;

        resolve(returnData);
      },(err)=>{
      let returnData = {};
      returnData[CONSTANTS.mail.err] = err;
      resolve(returnData);
    });


  });
};

function getOrCreateParent(criteria){
  return new Promise((resolve, reject)=>{
    const query  = {
      email : criteria.email,
      username : criteria.username
    };
    Users.findOne(query).exec((err, parent)=>{
      if(parent){
        resolve({object : parent, status: CONSTANTS.statuses.created});
      }else if(!err && !parent){
        const password = PasswordService.getRandomPassword();
        const parentToCreate = {
          'email' : criteria.email,
          'mobileNumber' : criteria.mobileNumber,
          'password' : password,
          'username' : criteria.username,
          'type' : CONSTANTS.userTypes.parent,
          'belongingTo' : criteria.school,
          'firstName' : criteria.firstName,
          'lastName' : criteria.lastName
        };
        Users.create(parentToCreate).exec(
          (function(createdPassword){
            return function(err, newParent){
              newParent.password = createdPassword;
              const parentMailType = CONSTANTS.emailMessageType.accCreated.name;
              const parentResults = {
                object : newParent,
                status : CONSTANTS.statuses.new
              };
              Promise.all([sendParentMailPromise( parentMailType, parentResults)]).then((results)=>{
                //Email has been sent to user, there is nothing to be done here
              },(err)=>{
                resolve(err);
              });
              resolve({object : newParent, status: CONSTANTS.statuses.new});
            }
          })(password)
        );
      }else{
        reject(err);
      }
    });
  });
}

function compareOldPassword(userId, oldPassword){
  return new Promise((resolve, reject)=>{
    Users.findOneById(userId).exec((err, userObj)=>{
      bcrypt.compare(oldPassword, userObj.password, function(err, res) {
        if(!err){
          resolve(res);
        }else{
          reject(err);
        }
      });
    })
  });
}

function insertPatchStudents(fullStudents, schoolId, locale){
  return new Promise((resolve, reject)=>{
    let users = [];
    let promises = [];
    for (var i = 0; i < fullStudents.length; i++) {
      let student = {};
      let parent = {};
      let date;


      //in case the file is in english
      if(fullStudents[i][CONSTANTS.patchInsert.en.First_name]){
        let year = fullStudents[i][CONSTANTS.patchInsert.en.Year];
        let month = fullStudents[i][CONSTANTS.patchInsert.en.Month];
        let day = fullStudents[i][CONSTANTS.patchInsert.en.Day];
        year = year ? parseInt(year) : "";
        month = month ? parseInt(month) : "";
        day = day ? parseInt(day) : "";

        if(year & month & day){
          let date =  new Date(year, month, day, 0 , 0, 0);
        }

        student.firstName = fullStudents[i][CONSTANTS.patchInsert.en.First_name];
        student.lastName = fullStudents[i][CONSTANTS.patchInsert.en.Full_father_name];
        student.birthDate = fullStudents[i][CONSTANTS.patchInsert.en.Birthdate];
        student.mobileNumber = fullStudents[i][CONSTANTS.patchInsert.en.Mobile_Number];
        student.homeNumber = fullStudents[i][CONSTANTS.patchInsert.en.Homenumber];
        student.address = fullStudents[i][CONSTANTS.patchInsert.en.Address];
        student.email = fullStudents[i][CONSTANTS.patchInsert.en.Email];
        student.birthDate = date ? date : undefined;
        student.type = 'student';
        student.belongingTo = schoolId;

        parent.firstName = fullStudents[i][CONSTANTS.patchInsert.en.Parent_name];
        parent.email = fullStudents[i][CONSTANTS.patchInsert.en.Parent_email];
        parent.mobileNumber = fullStudents[i][CONSTANTS.patchInsert.en.Parent_mobile_number];
        parent.job = fullStudents[i][CONSTANTS.patchInsert.en.Parent_job];
        parent.type = 'parent';
        parent.belongingTo = schoolId;
        promises.push(createStudentParent(student, parent));
      }else{
        let year = fullStudents[i][CONSTANTS.patchInsert.ar.Year];
        let month = fullStudents[i][CONSTANTS.patchInsert.ar.Month];
        let day = fullStudents[i][CONSTANTS.patchInsert.ar.Day];
        year = year ? parseInt(year) : "";
        month = month ? parseInt(month) : "";
        day = day ? parseInt(day) : "";

        if(year & month & day){
          let date =  new Date(year, month, day, 0 , 0, 0);
        }

        student.firstName = fullStudents[i][CONSTANTS.patchInsert.ar.First_name];
        student.lastName = fullStudents[i][CONSTANTS.patchInsert.ar.Full_father_name];
        student.birthDate = fullStudents[i][CONSTANTS.patchInsert.ar.Birthdate];
        student.mobileNumber = fullStudents[i][CONSTANTS.patchInsert.ar.Mobile_Number];
        student.homeNumber = fullStudents[i][CONSTANTS.patchInsert.ar.Homenumber];
        student.address = fullStudents[i][CONSTANTS.patchInsert.ar.Address];
        student.email = fullStudents[i][CONSTANTS.patchInsert.ar.Email];
        student.birthDate = date ? date : undefined;
        student.type = 'student';
        student.belongingTo = schoolId;

        parent.firstName = fullStudents[i][CONSTANTS.patchInsert.ar.Parent_name];
        parent.email = fullStudents[i][CONSTANTS.patchInsert.ar.Parent_email];
        parent.mobileNumber = fullStudents[i][CONSTANTS.patchInsert.ar.Parent_mobile_number];
        parent.job = fullStudents[i][CONSTANTS.patchInsert.ar.Parent_job];
        parent.type = 'parent';
        parent.belongingTo = schoolId;
        promises.push(createStudentParent(student, parent));

        // Promise.all(promises).then((results)=>{
        //   // let csvString = convertIntoCSV(results, 'ar');
        //   resolve(results);
        // }, (err)=>{
        //   reject(err);
        // });
      }
    }
    Promise.all(promises).then((results)=>{
      // let csvString = convertIntoCSV(results, 'en');
      resolve(results);
    }, (err)=>{
      reject(err);
    });
  });
}

function convertIntoCSV(jsons, locale){
  let fields;
if(locale == 'en'){
    fields = [
    CONSTANTS.patchInsert.en.First_name, CONSTANTS.patchInsert.en.Full_father_name,
    CONSTANTS.patchInsert.en.Mobile_Number, CONSTANTS.patchInsert.en.Homenumber, CONSTANTS.patchInsert.en.Address,
    CONSTANTS.patchInsert.en.Email, CONSTANTS.patchInsert.en.Email, CONSTANTS.patchInsert.en.UserName,
    CONSTANTS.patchInsert.en.Password, CONSTANTS.patchInsert.en.Parent_name, CONSTANTS.patchInsert.en.Parent_email,
    CONSTANTS.patchInsert.en.Parent_mobile_number, CONSTANTS.patchInsert.en.Parent_job, CONSTANTS.patchInsert.en.ParentUserName,
    CONSTANTS.patchInsert.en.ParentPassword
  ]
}else{
      fields = [
    CONSTANTS.patchInsert.ar.First_name, CONSTANTS.patchInsert.ar.Full_father_name, CONSTANTS.patchInsert.ar.Birthdate,
    CONSTANTS.patchInsert.ar.Mobile_Number, CONSTANTS.patchInsert.ar.Homenumber, CONSTANTS.patchInsert.ar.Address,
    CONSTANTS.patchInsert.ar.Email, CONSTANTS.patchInsert.ar.Email, CONSTANTS.patchInsert.ar.UserName,
    CONSTANTS.patchInsert.ar.Password, CONSTANTS.patchInsert.ar.Parent_name, CONSTANTS.patchInsert.ar.Parent_email,
    CONSTANTS.patchInsert.ar.Parent_mobile_number, CONSTANTS.patchInsert.ar.Parent_job, CONSTANTS.patchInsert.ar.ParentUserName,
    CONSTANTS.patchInsert.ar.ParentPassword
  ]
}
  let rows = jsons.map((json)=>{
    let jsonRow = {};
    if(locale == 'en'){
      jsonRow[CONSTANTS.patchInsert.en.First_name] = json.student.user.firstName;
      jsonRow[CONSTANTS.patchInsert.en.Full_father_name] = json.student.user.lastName;
      jsonRow[CONSTANTS.patchInsert.en.Birthdate] = json.student.user.birthDate;
      jsonRow[CONSTANTS.patchInsert.en.Mobile_Number] = json.student.user.mobileNumber;
      jsonRow[CONSTANTS.patchInsert.en.Homenumber] = json.student.user.homeNumber;
      jsonRow[CONSTANTS.patchInsert.en.Address] = json.student.user.address;
      jsonRow[CONSTANTS.patchInsert.en.Email] = json.student.user.email;
      jsonRow[CONSTANTS.patchInsert.en.UserName] = json.student.user.username;
      jsonRow[CONSTANTS.patchInsert.en.Password] = json.student.user.password;

      jsonRow[CONSTANTS.patchInsert.en.Parent_name] = json.parent.user.firstName;
      jsonRow[CONSTANTS.patchInsert.en.Parent_email]= json.parent.user.email;
      jsonRow[CONSTANTS.patchInsert.en.Parent_mobile_number] = json.parent.user.mobileNumber;
      jsonRow[CONSTANTS.patchInsert.en.Parent_job] = json.parent.user.job;
      jsonRow[CONSTANTS.patchInsert.en.ParentUserName] = json.parent.user.username;
      jsonRow[CONSTANTS.patchInsert.en.ParentPassword] = json.parent.user.password;
    }else{
      jsonRow[CONSTANTS.patchInsert.ar.First_name] = json.student.user.firstName;
      jsonRow[CONSTANTS.patchInsert.ar.Full_father_name] = json.student.user.lastName;
      jsonRow[CONSTANTS.patchInsert.ar.Birthdate] = json.student.user.birthDate;
      jsonRow[CONSTANTS.patchInsert.ar.Mobile_Number] = json.student.user.mobileNumber;
      jsonRow[CONSTANTS.patchInsert.ar.Homenumber] = json.student.user.homeNumber;
      jsonRow[CONSTANTS.patchInsert.ar.Address] = json.student.user.address;
      jsonRow[CONSTANTS.patchInsert.ar.Email] = json.student.user.email;
      jsonRow[CONSTANTS.patchInsert.ar.UserName] = json.student.user.username;
      jsonRow[CONSTANTS.patchInsert.ar.Password] = json.student.user.password;

      jsonRow[CONSTANTS.patchInsert.ar.Parent_name] = json.parent.user.firstName;
      jsonRow[CONSTANTS.patchInsert.ar.Parent_email]= json.parent.user.email;
      jsonRow[CONSTANTS.patchInsert.ar.Parent_mobile_number] = json.parent.user.mobileNumber;
      jsonRow[CONSTANTS.patchInsert.ar.Parent_job] = json.parent.user.job;
      jsonRow[CONSTANTS.patchInsert.ar.ParentUserName] = json.parent.user.username;
      jsonRow[CONSTANTS.patchInsert.ar.ParentPassword] = json.parent.user.password;
    }

    return jsonRow;
  });
  try {
    let csvStringResult = json2csv({ data: rows, fields: fields });
    console.log(csvStringResult);
    return csvStringResult;
  } catch (err) {
    // Errors are thrown for bad options, or if the data is empty and no fields are provided.
    // Be sure to provide fields if it is possible that your data array will be empty.
    console.error(err);
  }

}

function createStudentParent(student, parent){
  return new Promise((resolve, reject)=>{
    Sequence.create({}).exec((err, seq)=>{
      parent.username = "parent" + seq.id;
      parent.email = parent.email ? parent.email :  "parentEmail" +seq.id+"@gmail.com";
      parent.mobileNumber = parent.mobileNumber ? parent.mobileNumber : "000000001" + seq.id;
      student.username = "student" + seq.id;
      student.email = student.email ?student.email :  "studentEmail" +seq.id+"@gmail.com";
      createUser(parent).then((createdParent)=>{
        student.parent = createdParent.user.id;
        createUser(student).then((createdStudent)=>{
          resolve({student : createdStudent, parent : createdParent});
        }, (err)=>{
          reject(err);
        });
      }, (err)=>{
        reject(err);
      });
    });
  });
}

function getUserByMobileNumber(mobileNumber){
  return new Promise((resolve, reject)=>{
    Users.findOne({mobileNumber : mobileNumber}).exec((err, user)=>{
      if(err){
        return reject(err);
      }
      resolve(user);
    });
  });
}
