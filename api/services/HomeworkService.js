const Promise = require('promise');

exports.getClassHomeworks = getClassHomeworks;
function getClassHomeworks(classId, subjectId){
  return new Promise((resolve, reject)=>{
    const query = "select  users.id studentId, users.firstName, users.lastName, users.profileImage, hw.id homeworkId, stuhw.answerAttache, stuhw.id stuhwId "+
    "from users as users, homework as hw, studenthomework  as stuhw "+
    "where "+
    "users.id = stuhw.student and hw.id = stuhw.homework and hw.subject = " + subjectId;
    Homework.query(query, '', (err, rowResult)=>{
      if(err){
        return reject(err);
      }
      let answers = {};
      for (var i = 0; i < rowResult.length; i++) {
        if(!answers[rowResult[i].homeworkId]){
          answers[rowResult[i].homeworkId] = [];
        }
        answers[rowResult[i].homeworkId].push(rowResult[i]);
      }
      Homework.find({subject : parseInt(subjectId)}).exec((err, hws)=>{
        if(err){
          return reject(err);
        }
        for (var i = 0; i < hws.length; i++) {
          hws[i].answers = answers[hws[i].id];
        }
        resolve(hws);
      });

    });

    // Users.find({enrolledClass : classId}).exec((err, students)=>{
    //   if(err){
    //     return reject(err);
    //   }
    //
    //   Homework.find({subject : subjectId}).exec((err, homeworks)=>{
    //       if(err){
    //         return reject(err);
    //       }
    //
    //
    //   });
    });
}


exports.getStudentHomework = getStudentHomework;
function getStudentHomework(studentId, subjectId){
  return new Promise((resolve, reject)=>{
    const query = "select hw.name, hw.id hwId, hw.dueDate, stuhw.answerAttache, hw.questionAttache, stuhw.id stuhwId "+
    "from users as users, homework as hw, studenthomework  as stuhw "+
    "where "+
    "users.id = stuhw.student and hw.id = stuhw.homework and hw.subject = " + subjectId +" and users.id = " + studentId;
    Studenthomework.query(query, '', (err, rowResult)=>{
      if(err){
        return reject(err);
      }
      Homework.find({subject : subjectId}).exec((err, homeworks)=>{
        if(err){
          return reject(err);
        }
        let hwAnswers = UtilService.convertArrayIntoJson(rowResult, 'hwId');

        for (var i = 0; i < homeworks.length; i++) {
          homeworks[i].answer = hwAnswers[homeworks[i].id];
        }

        resolve(homeworks);
      });


    })
  });
}
