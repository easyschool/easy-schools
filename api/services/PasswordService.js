const generator = require('generate-password');

module.exports = {
  getRandomPassword : function(){
    const password = generator.generate({
        length: 10,
        numbers: true
    });
    return password;
  }
};
