const bcrypt = require('bcrypt');
module.exports = {
  parseMessage : function(message, args){
    for(let i = 1; i <= args.length; i++){
      message = message.replace(':'+i, args[i-1]);
    }
    return message;
  },
  matchedPlainHashPassword : function(plainPassword, hashPassword){
    if(plainPassword)
      return bcrypt.compareSync(plainPassword, hashPassword);
    else return false;
  },
  convertJsonIntoArrayValues : convertJsonIntoArrayValues,
  convertArrayIntoJson : convertArrayIntoJson,
  removeRedundantFromArrayOfJsons : function(array, key){
    bigJson = convertArrayIntoJson(array, key);
    array = convertJsonIntoArrayValues(bigJson);
    return array;
  },
  makeArrayWithWithGroupBrackets : function(array){
    if(array && Array.isArray(array)){
      let data = "(";
      for(let i = 0; i < array.length; i++){
        data += array[i] + ',';
      }
      data= data.substring(0, data.length - 1);
      data += ")";
      return data;
    }

    return null;
  },
  mergeArraysInArray : function(arrays){
    let array = [];
    for(let i = 0; i < arrays.length; i++){
      array = array.concat(arrays[i]);
    }
    return array;
  },
  getArrayValuesFromJsons : function(jsonList, field){

    let values = [];
    if(jsonList && Array.isArray(jsonList)){
      for(let i = 0; i < jsonList.length; i++){
        values.push(jsonList[i][field]);
      }
    }

    return values;
  },
  getTwoArraysDifferentiation : getTwoArraysDifferentiation,
  getFormattedDate : getFormattedDate
}
function getFormattedDate(date) {
    var year = date.getFullYear();
    /// Add 1 because JavaScript months start at 0
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return year+"-"+month+"-"+day
    // return month + '-' + day + '-' + year;
}
function convertJsonIntoArrayValues(jsonObj){
  let result = [];
  for(var i in jsonObj){
    result.push( jsonObj [i]);
  }
  return result;
}
function convertArrayIntoJson(array, key){
  let bigJson = {};
  for (var i = 0; i < array.length; i++) {
    bigJson[array[i][key]] = array[i];
  }
  return bigJson;
}

function getTwoArraysDifferentiation(newArray, oldArray, key){
  let newItems = [],  existingItems = [],  excludedItems = [];

  if(!oldArray){
    newItems = newArray;
    return {newItems, existingItems, excludedItems};
  }

  if(!newArray){
    existingItems = oldArray;
    return {newItems, existingItems, excludedItems};
  }

  const oldArrayJSON = convertArrayIntoJson(oldArray, key);
  const newArrayJSON = convertArrayIntoJson(newArray, key);



  //get new items
  for (var i = 0; i < newArray.length; i++) {
    if(!oldArrayJSON[newArray[i][key]]){
      newItems.push(newArray[i]);
    }else{
      existingItems.push(newArray[i]);
    }
  }

  //get excludedItems
  //get new items
  for (var i = 0; i < oldArray.length; i++) {
    if(!newArrayJSON[oldArray[i][key]]){
      excludedItems.push(oldArray[i]);
    }
  }

  return {newItems, existingItems, excludedItems};
}
