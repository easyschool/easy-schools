module.exports = {

  attributes: {
    'student' : {
      model : 'users'
    },
    'grade' : {
      model : 'grades'
    },
    'score' : {
      type: 'integer',
      required: true
    }
  }
};
