/**
 * LogReport.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    timeSpent : 'string',
    enterDate : 'datetime',
    lastVisitDate : 'datetime',
    page : 'string',
    type : 'string',
    user : {
      model : 'users'
    },
    school : {
      model : "schools"
    }
  }

};
