module.exports = {

  attributes: {
    announce : {
      model : 'announcements'
    },
    'division' : {
      model : 'divisions'
    }
  }
};
