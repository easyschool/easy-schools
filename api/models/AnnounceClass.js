module.exports = {

  attributes: {
    'announce' : {
      model : 'announcements'
    },
    'class' : {
      model : 'classes'
    }
  }
};
