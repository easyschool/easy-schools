module.exports = {

  attributes: {
    'student' : {
      model : 'users'
    },
    'homework' : {
      model : 'homework'
    },
    'answerAttache' : 'string'
  }
};
