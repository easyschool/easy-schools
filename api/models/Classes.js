/**
 * Classes.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models

 */

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true
    },
    schedule: {
      type: 'string',
      required: true
    },
    //Old code must leave it alone
    division: {
      type: 'string'
    },
    belongingTo: {
      type: 'string',
      required: true
    },
    division_key :{
      model : "divisions"
    },
    students : {
      collection: 'users',
      via: 'enrolledClass'
    },
    // Add a reference to Subject
    subjects: {
      collection: 'subjects',
      via: 'class'
    },
    announces : {
      collection : 'announcements',
      via : 'classes',
      through : "announceclass"
    },
    'masters':{
       collection: 'users',
       via: 'masterClasses',
       through : 'classmaster'
    },
    'creator' : {
      model : 'users'
    }
  },
  // Lifecycle Callbacks
  afterDestroy: function (values, cb) {
    let calssesIds = [];
    for(let i = 0; i < values.length; i++){
      calssesIds.push(values[i].id);
    }
    //Remove corresponding subjects and students
    const removeSubjectsPromise = new Promise((resolve, reject)=>{
      Subjects.destroy({"class" : calssesIds}).exec((err)=>{
        if(!err){
          resolve('success remove subjects');
        }else{
          console.log(err);
          reject(err);
        }
      });
    });


    //stop deleteing the students in case of delete class or division

    // const removeStudentsPromise = new Promise((resolve,reject)=>{
    //   Users.destroy({"enrolledClass" : calssesIds}).exec((err)=>{
    //     if(!err){
    //       resolve('success remove students');
    //     }else{
    //       console.log(err);
    //       reject(err);
    //     }
    //   });
    // })

    const removeClassAnnounce = new Promise((resolve, reject)=>{
      AnnounceClass.find({'class' : calssesIds}).exec((err, announceClasses)=>{
        if(announceClasses.length === 1){
          Announcements.destroy({'id' : announceClasses[0].announce}).exec((err)=>{
            if(!err){
              AnnounceClass.destroy({'class' : calssesIds}).exec((err)=>{
                if(!err){
                  resolve('announces related to classes are removed.');
                }else{
                  console.log(err);
                  reject(err);
                }
              });
            }
          })
        }else{
          AnnounceClass.destroy({'class' : calssesIds}).exec((err)=>{
            if(!err){
              resolve('announces related to classes are removed.');
            }else{
              console.log(err);
              reject(err);
            }
          });
        }
      });

    });

    Promise.all([removeClassAnnounce, removeSubjectsPromise]).then((results)=>{
      cb();
    },(err)=>{
      console.log(err);
      cb();
    });
  }
};
