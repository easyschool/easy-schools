module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true
    },
    belongingTo: {
      type: 'string'
    },
    "classes" : {
      collection: 'classes',
      via: 'division_key',
    },
    "announces" : {
      collection : 'announcements',
      via : 'divisions',
      through : 'announcedivision'
    },
    school: {
      model : 'schools'
    },
    'creator' : {
      model : 'users'
    }
  },
  // Lifecycle Callbacks
  afterDestroy: function (values, cb) {

    let divisionIds = [];
    for(let i = 0; i < values.length; i++){
      divisionIds.push(values[i].id);
    }

    Classes.destroy({division_key : divisionIds}).exec((err)=>{

      cb();

    });

  }

};
