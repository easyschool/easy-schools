/**
 * Grades.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    'name': {
      type: 'string',
      required: true
    },
    'maxScore':{
      type: 'integer',
      required: true
    },
    'subject' : {
      model : 'subjects'
    },
    'students' : {
      collection : 'users',
      via : 'grades',
      through : "usergrade"
    }
  },
  // Lifecycle Callbacks
  afterDestroy: function (values, cb) {
    let gradesIds = [];
    for(let i = 0; i < values.length; i++){
      gradesIds.push(values[i].id);
    }
    UserGrade.destroy({grade : gradesIds}).exec((err)=>{
      if(!err){
        return cb();
      }
      cb(err);
    });
  }
};
