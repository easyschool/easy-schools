/**
 * Subjects.js
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.

 */
module.exports = {

  attributes: {

    name: {
      type: 'string',
      required: true
    },
    //belong to class
    belongingTo: {
      type: "string",
      required: true
    },
    // Add a reference to Users
    users: {
      collection: 'users',
      via: 'id',
      dominant: true
    },
    grades : {
      collection: 'grades',
      via: 'subject'
    },
    homeworks : {
      collection: 'homework',
      via: 'subject'
    },
    // Add a reference to class
    class: {
      model: 'classes'
    },
    students: {
     collection: 'users',
     via: 'optionalSubjects',
     through : 'studentsubjects'
   },
    optional : {
      type : "boolean",
      defaultsTo : false
    }

  }

};
