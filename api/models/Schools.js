/**
 * Schools.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true
    },
    schoolLogo: {
      type: 'string',
      required: true
    },
    locale: {
      type: 'string',
      // required: true
    },
    divisions: {
      collection: 'divisions',
      via: 'school'
    }
  }
};


// School -> Classes -> Subjects -> Attendance/HW/Grades (Ignoring ClassLevels for now)
// Classes only belong to Schools
// Subjects only belong to Classes
// Attendance/HW/Grades belong to both Subject and User
