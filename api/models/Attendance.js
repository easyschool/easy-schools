/**
 * Attendance.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    date: {
     type: 'date',
     required: true
    },
    attended: {
      type: 'boolean',
      required: true,
      defaultsTo: false
    },
    belongingTo: {
      type: 'string',
      required: true
    },
    inSubject: {
      type: 'string',
      required: true
    },
    subject:{
      model : "subjects"
    },
    student : {
      model : "users"
    }
  }
};
