/**
 * Homework.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true
    },
    dueDate: {
      type: 'date',
      required: true
    },
    description: {
      type: 'string'
    },
    'questionAttache' : 'string',
    'subject' : {
      model : 'subjects'
    },
    'students' : {
      collection : 'users',
      via : 'homeworks',
      through : "studenthomework"
    }
  },
  // Lifecycle Callbacks
  afterDestroy: function (values, cb) {

    let ids = [];
    for(let i = 0; i < values.length; i++){
      ids.push(values[i].id);
    }

    Studenthomework.destroy({homework : ids}).exec((err)=>{

      cb();

    });

  }
};
