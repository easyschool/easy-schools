module.exports = {

  attributes: {
    'student' : {
      model : 'users'
    },
    'subject' : {
      model : 'subjects'
    }
  }
};
