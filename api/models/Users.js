/**
 * Users.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var bcrypt = require('bcrypt');


module.exports = {

  attributes: {
    firstName: {
      type: 'string'
    },
    lastName: {
      type: 'string'
    },
    username: {
      type: 'string',
      unique: true,
      required: true
    },
    password: {
      type: 'string',
      required: true
    },
    profileImage: {
      type: 'string'
    },
    birthDate: {
      type: 'date'
    },
    mobileNumber: {
      type: 'string'
    },
    homeNumber:{
      type: 'string'
    },
    address : {
      type : 'string'
    },
    email: {
      type: 'email',
      // unique: true
    },
    type: {
      type: 'string',
      required: true,
      enum: ['superuser', 'admin', 'teacher', 'student', 'parent']
    },
    classes: {
      type: 'array'
    },
    subjects: {
      type: 'array'
    },
    belongingTo: {
      type: 'string'
      //required: true
    },
    'enrolledClass' : {
      model : 'classes'
    },
    myannounces : {
      collection: 'announcements',
      via: 'owner'
    },
    payments: {
      type: 'string'
    },
    lastSignedin: {
      type: 'date'
    },
    parent : {
      model : 'users'
    },
    children :{
      collection : 'users',
      via : 'parent'
    },
    deviceId: {
      type: 'string'
    },
    announces : {
      collection: 'announcements',
      via: 'users',
      through : 'announceuser'
    },
    'masterClasses':{
       collection: 'classes',
       via: 'masters',
       through : 'classmaster'
    },
    grades : {
      collection : 'grades',
      via : 'students',
      through : "usergrade"
    },
    homeworks : {
      collection : 'homework',
      via : 'students',
      through : "studenthomework"
    },
    optionalSubjects: {
     collection: 'subjects',
     via: 'students',
     through : 'studentsubjects'
    }
  },
  beforeCreate: function(user, cb) {
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) cb(err);
         else {
         user.password = hash;
          cb();
        }
      });
    });
  },

    beforeUpdate: function(user, cb) {
     //console.log("user.password :"+user.password);
      if(user.password){
        bcrypt.genSalt(10, function(err, salt) {
          if (err) return cb(err);
          bcrypt.hash(user.password, salt, function(err, crypted) {
            if (err)  cb(err);
            else {
              if(!user.password.includes("$2a$")){
                  user.password = crypted;
              }

              cb();
            }
          });
        });
     }
     else
      return cb();
  },
  // Lifecycle Callbacks
  afterDestroy: function (values, cb) {

    let usersIds = [];
    for(let i = 0; i < values.length; i++){
      usersIds.push(values[i].id);
    }

    //Delete from announceuser, usergrade

    UserGrade.destroy({student : usersIds}).exec((err)=>{
      AnnounceUser.destroy({user : usersIds}).exec((err)=>{
        cb();
      });
    });


  }

};
