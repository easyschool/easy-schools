/**
 * Announcements.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

const Promise = require('promise');

module.exports = {

  attributes: {

    title: {
      type: 'string',
      required: true
    },
    description:{
      type: 'string'
    },
    announce_type: {
      type: 'string',
      required: 'true',
      'defaultsTo' : 'public'
    },
    'owner' : {
      model : 'users'
    },
    school: { // School to which it belongs
        model: 'schools',
        required : 'true'
    },
    url: {
      type: 'string'
    },
    start: {
      type: 'date'
    },
    end: {
      type: 'date'
    },
    classes: {
     collection: 'classes',
     via: 'announces',
     through : 'announceclass'
    },
    users:{
       collection: 'users',
       via: 'announces',
       through : 'announceuser'
    },
    visibility:{
      type: 'string',
      enum: ['public', 'private']
    }
  },

  afterDestroy: function (values, cb) {
    let announceIds = [];
    for(let i = 0; i < values.length; i++){
      announceIds.push(values[i].id);
    }

    const removeAnnounceClassPromise = new Promise((resolve, reject)=>{
      AnnounceClass.destroy({announce : announceIds}).exec((err)=>{
        if(!err){
          resolve('announce class deleted successfully');
        }else{
          reject(err);
        }
      });
    });

    const removeAnnounceUserPromise = new Promise((resolve, reject)=>{
      AnnounceUser.destroy({announce : announceIds}).exec((err)=>{
        if(!err){
          resolve('announce user deleted successfully');
        }else{
          reject(err);
        }
      });
    });

    const removeAnnounceDivPromise = new Promise((resolve, reject)=>{
      AnnounceDivision.destroy({announce : announceIds}).exec((err)=>{
        if(!err){
          resolve('announce division deleted successfully');
        }else{
          reject(err);
        }
      });
    });

    Promise.all([removeAnnounceClassPromise, removeAnnounceUserPromise, removeAnnounceDivPromise]).then((results)=>{
      cb();
    }, (err)=>{
      cb(err);
    })
  }
};
