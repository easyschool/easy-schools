module.exports = function(req, res, next) {
	if (req.session.lang === undefined) req.session.lang = 'en'
	req.setLocale(req.session.lang);
	next();
};
