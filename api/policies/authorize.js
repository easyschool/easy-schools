module.exports = function (userRoles) {
  return function (req, res, next) {

  if(!req.user){
    return res.redirect("/login");
  }

  if(userRoles.indexOf(req.user.type) >= 0 ){
    return next();
  }

  if (req.user.type == "superuser"){

    res.redirect('/schools');
  }
  if (req.user.type == "admin"){

    res.redirect('/schools/' + req.user.belongingTo);
  }
  if (req.user.type == "teacher"){
    Schools.findOneById(req.user.belongingTo).exec(function(err, sch) {
      sails.config.global_data.school_name = sch.name;
      res.redirect('/schools/' + req.user.belongingTo + '/users/' + req.user.id + '/home?userType=' + req.user.type);
    });
  }


  if(req.user.type === 'parent' || req.user.type == "student"){
    res.redirect('/schools/' + req.user.belongingTo + '/users/' + req.user.id + '/home?userType=' + req.user.type);
  }
}
}
