/**
 * AnnouncementsController
 *
 * @description :: Server-side logic for managing announcements
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const CONSTANTS = sails.config.constants;
module.exports = {
    renderAnnouncements: function(req, res) {
      var currentDate = new Date();
      var currentMonth = currentDate.getMonth();
      var currentYear = currentDate.getFullYear();
      var startDate = new Date(currentYear, currentMonth);
      var endDate = new Date(currentYear, currentMonth+1);
      var schoolId = parseInt(req.param('school_id'));

      AnnouncementService.getAnnouncementsDateRange(startDate, endDate, schoolId).then((announces)=>{

        res.view('announcments/announcments',{
          "announces" : announces,
          "school_id":req.param('school_id')
        });
      },(err)=>{
        return res.serverError(err);
      });
    },
    renderAnnouncementsNew: function(req, res) {
      const schoolId = req.param('school_id');
        Classes.findByBelongingTo(schoolId).exec(function(err, classes) {
            Users.findByBelongingTo(schoolId).exec(function(err, users) {
                res.view('announcments/announcments_new', {
                    classes_list: classes,
                    students_list: users
                });
            });
        });
    },
    createAnnouncement: function(req, res) {
        var date_got = new Date(req.param('dueDate'));
        var date_converted = date_got.getTime();
        var announceObj = req.body;
        if(announceObj.announce_type === CONSTANTS.announce.types.student || announceObj.announce_type === CONSTANTS.announce.types.parent ){
          delete announceObj.classes;
        }
        announceObj.owner = req.user.id;
        AnnouncementService.createAnnouncement(announceObj).then((announceCreated)=>{
          res.redirect("schools/"+ req.param('school_id')+ "/announce");
        },(err)=>{
          console.log(err);
          return res.serverError(err);
        });
    },
    getAnnounces : function(req, res){
      var startDate = req.query.startdate;
      var endDate = req.query.enddate;
      var schoolId = req.query.schoolId;

      AnnouncementService.getAnnouncementsDateRange(startDate, endDate, schoolId).then((announces)=>{
        return res.json(announces);

      },(err)=>{
        return res.serverError(err);
      });
    },
    getMyAnnouncements : function(req, res){
      let startDate = req.query.startdate;
      let endDate = req.query.enddate;
      let schoolId = req.query.schoolId;
      //this field for mobile request
      let userId = req.query.userId;

      //In case the user is super user
      if(schoolId && req.user.type === CONSTANTS.userTypes.superuser){
        AnnouncementService.getAnnouncementsDateRange(startDate, endDate, schoolId).then((announces)=>{
          return res.json(announces);

        },(err)=>{
          return res.serverError(err);
        });
      }

      if(userId){
        Users.findOne(userId).exec((err, user)=>{
          AnnouncementService.getAssociatedAnnounces(user, startDate, endDate).then((announces)=>{
            res.json(announces);
          },(err)=>{
            console.log(err);
            Utils.sendError(res,err);
          });
        });
      }else{
        AnnouncementService.getAssociatedAnnounces(req.user, startDate, endDate).then((announces)=>{
          res.json(announces);
        },(err)=>{
          console.log(err);
          Utils.sendError(res,err);
        });
      }



    }
};
