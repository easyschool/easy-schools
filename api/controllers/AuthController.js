/**
 * UsersController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var passport = require("passport");
var dateFormat = require('dateformat');
const CONSTANTS = sails.config.constants;
const MESSAGES = sails.config.messages;

var Promise = require('promise');
module.exports = {
  forgetPassword : function(req, res){

      const email = req.body.email;
      const mobileNumber = req.body.mobileNumber;
      const username = req.body.username;
      const target = req.query.target;
      let query ;
      if(email){
          query = { "email" : email };
      }else if(username){
        query = { "username" : username };
      }else if(mobileNumber){
        query = { "mobileNumber" : mobileNumber };
      }else{
        return res.send('Un supported operation');
      }



        Users.find(query).exec((err, user)=>{
          if(user && user.length > 0){
            let miniUser = {
              email : user[0].email,
              "fakeData" : user[0].password
            };
            token = SecurityUtil.generateJWTBasedOnTime(miniUser, CONSTANTS.forget_password_link_timeout);

            let url;

            if(process.env.NODE_ENV === 'local'){
              url = sails.config.constants.url.local;
            }else if(process.env.NODE_ENV === 'development'){
              url = sails.config.constants.url.development;
            }else{
              url = sails.config.constants.url.production;
            }

            const mailType = CONSTANTS.emailMessageType.forgetPassword.name;
            MailService.sendEmail(mailType, user[0].email, {'user' : user[0], token : token, url : url, schoolId: user[0].belongingTo}).then((result)=>{

              if(target){
                return res.send({success : MESSAGES.users.forget_password_email_sent});
              }

              return res.view('password/response-sent-email',{
                message : {success : MESSAGES.users.forget_password_email_sent}
              });

            }, (err)=>{
              console.log(err);
            });
          }else if(!err){
            if(target){
              return res.send({error : MESSAGES.email_not_found});
            }
            return res.view('password/response-sent-email',{
              message : {error : MESSAGES.email_not_found}
            });
          }else{
            if(target){
              return res.send({error : err});
            }

          }
        })


  },
  resetPassword : function(req, res){
    let info = req.body;
    if((!info.password || !info.confirmPassword )){
      return res.view('password/reset-password',{
        message : {error : messages.password_confirm_not_exist}
      });
    }
    if((info.password !== info.confirmPassword)){
      return res.view('password/reset-password',{
        message : {error : messages.password_confirm_error}
      });
    }
    if(!info.token){
      return res.view('password/reset-password',{
        message : {error : messages.not_authorized_action}
      });
    }
    SecurityUtil.verifyToken(info.token).then((user)=>{
      Users.update({email : user.email}, {password : info.password}).exec((err, userUpdated)=>{
        if(!err){
          return res.view('password/response-sent-email',{
            message : {success : MESSAGES.change_password_success}
          });
        }else{
          res.send(err)
        }
      });
    }, (err)=>{
      return res.view('password/response-sent-email',{
        message : {error : messages.invalid_reset_password_token}
      });
    });

  },
  login: function (req, res) {
    passport.authenticate('local', function (err, user, info) {
      // return res.redirect("/");
      if ((err) || (!user)) {
        req.flash("message", info.message);
        return res.redirect("/login")
      }
      req.logIn(user, function (err) {
        if (err) req.flash("message", err);

        Schools.findOneById(user.belongingTo).exec((err, school)=>{
          req.session.school = school;
          return res.redirect("/");
        });

      });
    })(req, res);
  },
  logout: function (req, res) {
    req.logout();
    res.redirect("/");
  },
  renderLogin: function (req, res) {
    var m = req.flash("message")[0]
    return res.view('login', {
      message: m
    });
  },

  renderForgetPassword: function (req, res) {
    var m = req.flash("message")[0]
    return res.view('password/forget-password');
  }
  //
  // renderLogin: function (req, res) {
  //   var m = req.flash("message")[0]
  //   return res.view('login', {
  //     message: m
  //   });
  // }
}
