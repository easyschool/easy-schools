/**
* ClassesController
*
* @description :: Server-side logic for managing classes
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/
const CONSTANTS = sails.config.constants;
module.exports = {
renderClass: function (req, res) {
  Classes.findOneById(req.param('class_id')).exec(function (err, found) {
   console.log(req.param('class_id'));
    if (found) sails.config.global_data.class_name = found.name;
    else sails.config.global_data.class_name = "";

    // Schools.findOneById(found.belongingTo).exec((err, school)=>{
      console.log(found);
      res.view('classes/class', {
        class_found: found
        // school : school
      });
    // });

  })
},
renderClassesNew: function (req, res) {
  Divisions.findByBelongingTo(req.param('school_id')).exec(function (err, divisions) {
    res.view('classes/classes_new', {
      divisions_list: divisions
    });
  })
},
renderClassSchedule: function (req, res) {
  Classes.findOneById(req.params.class_id).exec((err, classObj)=>{
    if(!err){
      return res.view('classes/schedule', {
        classObj: classObj
      });
    }
    Utils.sendError(res, err);

  });
  // Divisions.findByBelongingTo(req.param('school_id')).exec(function (err, divisions) {
  //   res.view('classes/classes_new', {
  //     divisions_list: divisions
  //   });
  // })
},
renderClassesRemove: function (req, res) {
  res.view('classes/classes_remove');
},
renderClassUpdate: function (req, res) {
  Classes.findOne(req.params.class_id).exec((err, classFound)=>{
    if(!err){
        return res.view('classes/class_update',{
          class_found : classFound
        });
    }
    Utils.sendError(res, err);
  });
},
createClass: function (req, res) {

 console.log("divisions :"+req.param("division"));


Classes.create({
    name: req.param("name"),
    belongingTo: req.param("school_id"),
    division: req.param("division"),
    schedule: req.param("schedule"),
    "division_key" : parseInt(req.param("division")),
    'creator' : req.user.id
  }).exec(function createCB(err, created) {
    if (err) return res.serverError(err);
    else return res.redirect("/schools/" + req.param('school_id') + "/years");
  });


},
updateClass: function (req, res) {

  console.log("divisions :"+req.param("division"));



       let classObj = {
         name: req.param("name")
       };
       if(req.param("schedule")){
         classObj.schedule = req.param("schedule");
       }

   Classes.update(req.param('class_id'), classObj).exec(function createCB(err, updated) {
     if (err) return res.serverError(err);
     else return res.redirect("/schools/" + updated[0].belongingTo + "/classes/" + updated[0].id);
   });

},

removeClass: function (req, res) {
  Classes.destroy(req.param('class_id')).exec(function(err) {
    if (err) return res.send(err);
    else return res.redirect('/schools/');
  }) },

  getClasses : function(req, res){
    const ids = req.query.ids;
    const userId = req.query.userId;
    const type = req.query.type;
    if(ids){
      ClassService.getClasses(ids).then((classes)=>{
        res.send(classes);
      },(err)=>{
        Utils.sendError(res, err);
      });
    }else if(userId && type === CONSTANTS.userTypes.admin ){

      ClassService.getAdminClasses(req.user.classes, req.user.id).then((divisionsList)=>{
        res.send(divisionsList);
      }, (err)=>{
          Utils.sendError(res, err);
      });
    }else if(userId && type === CONSTANTS.userTypes.teacher ){
      Users.findOneById(userId).exec((err, userObj)=>{
        Subjects.find({id : userObj.subjects}).exec((err, subjects)=>{
          let classesIds = [];
          for(let i = 0; i < subjects.length; i++){
            classesIds.push(subjects[i].class);
          }
          Classes.find({id : classesIds}).populate('division_key').exec((err, classes)=>{

            const expandedDivision = convertClassesIntoExpandedDiv(classes);
            res.send(UtilService.convertJsonIntoArrayValues(expandedDivision));
          });
        });
      });
    }else{
        res.send({'status' : 'please provide ids or masterId paramater'});
    }

  }
};

function convertClassesIntoExpandedDiv(classes){
  let divisionsJson = {};
  let tempDivision, tempClass;
  for(let i = 0; i < classes.length; i++){
    tempClass = classes[i];
    tempDivision = divisionsJson[tempClass.division_key.id];
    if(tempDivision && tempDivision.classes){
      tempDivision.classes.push(tempClass);
    }else if(tempDivision && !tempDivision.classes){
      tempDivision.classes = [];
      tempDivision.classes.push(tempClass);
    }else{
      divisionsJson[tempClass.division_key.id] = tempClass.division_key.toJSON();
      divisionsJson[tempClass.division_key.id].classes = [];
    }

    divisionsJson[tempClass.division_key.id].classes.push(tempClass);
  }
  return divisionsJson;
}
