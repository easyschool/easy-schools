/**
 * SubjectsController
 *
 * @description :: Server-side logic for managing subjects
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  renderSubjects: function (req, res) {
    const boardItem = req.query.boardItem;
    Subjects.findByBelongingTo(req.param('class_id')).exec(function (err, subjects_f) {
      console.log(err);
      console.log(subjects_f);
      if(!err){
        return res.view('subjects/subjects', {
          subjects_list: subjects_f,
          boardItem : boardItem
        });
      }
      Utils.sendError(res, err);



    })
  },
  renderStudentSubjects :  function (req, res) {
    const boardItem = req.query.boardItem;

    Subjects.findByBelongingTo(req.param('class_id')).populate('students').exec(function (err, subjects_f) {

      let subjects = subjects_f.filter((subject)=>{
        if(subject.optional){
          for (var i = 0; i < subject.students.length; i++) {
            if(subject.students[i].id === req.user.id){
              return true;
            }
          }
          return false;
        }else{
          return true;
        }
      });

      res.view('subjects/student_subjects', {
        subjects_list: subjects,
        boardItem : boardItem
      });
    });
  },
  renderSubjectsNew: function (req, res) {
    const classId = req.params.class_id;
    Users.find({enrolledClass : classId}).exec((err, students)=>{
      if(!err){
        return res.view('subjects/subjects_new', {
          students : students
        });
      }

      Utils.sendError(res, err);

    });

  },
  renderSubject: function (req, res) {
    res.view('subjects/subject');
  },
  renderSubjectsList: function (req, res) {
    Classes.find().exec(function (err, classes) {
    /*  var class_ids = [];
      for (i = 0; i < classes.length; i++) {
        class_ids.push(classes[i].id);
      }*/
      Users.findOne(req.param('user_id')).exec(function (err, found) {
        Subjects.find({ id: found.subjects }).exec(function (err, subjects_list) {

          res.view('subjects/subject_list', {
            user: found,
            subjects_list: subjects_list,
            classes_list: classes
          });

        });
      });
    });
  },
  renderSubjectUpdate: function (req, res) {
    const classId = req.params.class_id;
    Users.find({enrolledClass : classId}).exec((err, students)=>{
      if(!err){
        return Subjects.findById(req.param('subject_id')).populate('students').populate('class').exec((err, subject)=>{
          if(!err){
            let enrolledStudents = UtilService.convertArrayIntoJson(subject[0].students, 'id');
            return res.view('subjects/subject_update', {
              students : students,
              enrolledStudents: enrolledStudents,
              subject : subject.length > 0? subject[0] : {name : ""}
            });
          }
        });

      }

      Utils.sendError(res, err);

    });

  },

  updateSubject : function(req, res){
    let name = req.body.name;
    let id = parseInt(req.params.subject_id);
    let oldCheckStudentsIds = req.body.oldCheckStudentsIds;
    let studentIds = req.body.studentIds;
    Subjects.update(id, {name : name}).exec((err, updatedSubject)=>{
      if(!err && oldCheckStudentsIds && studentIds){
        for(let i = 0; i < oldCheckStudentsIds.length; i++){
          if(!studentIds.includes(oldCheckStudentsIds[i])){
            updatedSubject[0].students.remove(oldCheckStudentsIds[i]);
          }
        }
        updatedSubject[0].students.add(studentIds);
        updatedSubject[0].save();

        return res.send(updatedSubject);
      }else if(!err){
        return res.send(updatedSubject);
      }

      Utils.sendError(res, err);
    });
  },
  createSubject: function (req, res) {
    const subject = req.body;
    subject.belongingTo = req.param("class_id");
    subject.class = parseInt(req.param("class_id"));
    subject.optional = subject.optional? 1 : subject.optional;
    Subjects.create(subject).exec(function createCB(err, created) {
      if (err) return res.serverError(err);

      else {
        if(subject.optional){
          return res.send(created);
        }

        return res.redirect("/schools/" + req.param('school_id') + "/classes/" + req.param('class_id') + "/subjects");
      };
    });
  },

  deleteSubject: function (req, res) {
    Subjects.destroy({ id: req.param("subject_id") }).exec(function createCB(err, created) {
      if (err) return res.serverError(err);
      else return res.redirect("/schools/" + req.param('school_id') + "/classes/" + req.param('class_id') + "/subjects");
    });
  },
  assignStudentToOptionalSub: function(req, res){
    let studentSubjects = req.body.enrolledStudents;

    SubjectService.addOptionalStdudentSubjects(studentSubjects).then((newObjects)=>{
      res.send(newObjects);
      // return res.redirect("/schools/" + req.session.school.id + "/classes/" + req.param('class_id') + "/subjects");
    }, (err)=>{
      Utils.sendError(res, err);
    });
  },
  updateStudentSubjects : function(req, res){
    let newStudentSubjects = req.body;
    let subjectId = req.params.subject_id;
    Studentsubjects.find({subject : subjectId}).then((studentSubjects)=>{
      let {newItems, existingItems, excludedItems} = UtilService.getTwoArraysDifferentiation(newStudentSubjects, studentSubjects, 'id');
      SubjectService.removeStudentSubjects(excludedItems).then((result)=>{
        SubjectService.addOptionalStdudentSubjects(newItems).then((result)=>{
          res.send('Data has been updated');
        }, (err)=>{
          Utils.sendError(res, err);
        });
      }, (err)=>{
        Utils.sendError(res, err);
      });
    }, (err)=>{
      Utils.sendError(res, err);
    });
  }

};
