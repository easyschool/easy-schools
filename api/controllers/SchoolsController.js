 /**
 * SchoolsController
 *
 * @description :: Server-side logic for managing schools
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  renderSchools: function(req, res) {

    Schools.find().exec(function(err, schools) {
      if (err) return res.serverError(err);
      else return res.view('schools/schools', {
        school_list: schools
      });
    });
  },
  renderNewSchool: function(req, res) {
    res.view('schools/schools_new');
  },
  renderRemoveSchool: function(req, res) {
    Schools.find().exec(function(err, schools) {
      if (err) return res.serverError(err);
      else return res.view('schools/schools_remove', {
        schools: schools
      });
    });
  },
  renderSchool: function(req, res) {
    if(req.param('school_id') && req.param('school_id') !== 'undefined'){
      Schools.findOne(req.param('school_id')).exec(function(err, found) {
      console.log(err);
      if (err) {
        return res.send(err);
      }else {
        if (found) {
          sails.config.global_data.school_name = found.name;
          sails.config.global_data.school_image = found.schoolLogo;}
          req.session.school = found;
          return res.view('schools/school', {
            school: found
          });
      }
      });
    }
  },
  renderUpdateSchool: function(req, res) {
    Schools.findOne(req.param('school_id')).exec(function(err, found) {
      if (err) return res.send(err);
      else return res.view('schools/school_update', {
        school: found
      });
    });
  },

  createNewSchool: function(req, res) {



        const schoolObj = {
          name: req.param("name"),
          schoolLogo: req.param("schoolLogo")
        };
        // res.json({'name' : 'happy'})
        console.log(schoolObj);
        Schools.create(schoolObj).exec(function createCB(err, created) {
          if (err) return res.serverError(err);
          else return res.redirect("/schools");
        });

  },
  removeSchool: function(req, res) {
    // this should also remove users, Subjects and anything associated with it
    Schools.destroy(req.param('school_id')).exec(function(err) {
      if (err) return res.send(err);
      else return res.redirect('/schools/remove');
    })

  },

  updateSchool: function(req, res) {

        var updatedObject = {
          name: req.param("name"),
          schoolLogo : req.param("schoolLogo")
        };


        Schools.update(req.param('school_id'), updatedObject).exec(function createCB(err, created) {
          if (err) return res.serverError(err);
          else return res.redirect("/schools/" + req.param('school_id'));
        });


  }


};
