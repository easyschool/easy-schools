module.exports = {
	setLocale: function(req, res) {
		req.session.lang = req.param('lang');
		req.locale = req.session.lang;
		res.redirect(req.get('referer'));

	}
};
