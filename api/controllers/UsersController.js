/**
 * UsersController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const EJS = require('ejs');
var dateFormat = require('dateformat');
const CONSTANTS = sails.config.constants;
const MESSAGES = sails.config.messages;
var Promise = require('promise');
var multer  = require('multer');

var upload = multer({ storage: multer.memoryStorage() });
const csv=require('csvtojson');

module.exports = {
  renderUsers: function (req, res) {
    if (req.param('school_id') == "undefined") return res.redirect('/');
    var types = req.query.type;
    Users.find({ belongingTo: req.param('school_id')}).where({ type: types }).populate('enrolledClass').exec(function (err, users) {

      if (err) return res.serverError(err);
      else {
        return res.view('users/users', {
          users_list: users
        });
      }
    });
  },
  renderUserHomePage : function(req, res){
    //Handle parent info to render page
    let userType = req.query.userType;
    switch (userType) {
      case CONSTANTS.userTypes.parent:
        Users.findOneById(req.params.user_id).populate('children').exec((err, user)=>{
          if(user.children && user.children.length === 1){
            return res.view('users/student',{
              "student" :  user.children[0]
            });

            // return res.redirect('/schools/' + user.children[0].belongingTo + '/classes/' + user.children[0].enrolledClass);
          }else{

            return res.view('users/parent',{
              "parent" : user
            });
          }
        });

        break;
      case CONSTANTS.userTypes.teacher:
        res.view('users/teacher.ejs');
        break;
      case CONSTANTS.userTypes.student:

        Users.findOneById(req.params.user_id).exec((err, user)=>{
          return res.view('users/student',{
            "student" : user
          });
        });

        break;
      default:

    }

  },
  renderStudentHomePage : function(req, res){

  },
  userImage : function(req, res){
    const target = req.query.target;
    let user = {
      profileImage : req.param('profileImage')
    };
    Users.update(req.param('user_id'), user).exec((err, result)=>{
      if(!err){
        if(target){
          return res.send("Image uploaded successfully");
        }
        return res.redirect('/');
        // return res.send('success');
      }
      Utils.sendError(res, err);
    });
  },
  renderMobProfileImage : function(req, res){
    const id = req.query.userid;
    Users.findOneById(id).exec((err, user)=>{
      if(!err){
        let path =   __dirname + '/../../views/profile/mob-profile.ejs';
        EJS.renderFile(path, {user: user}, {}, function(err, str){
          if(!err)
            return res.send(str);
          else {
            return res.send(err);
          }
        });


      }else {
          Utils.sendError(res, err);
      }

    });
  },
  createUser: function (req, res) {
    let masteredClasses = req.param('classes');
    let subjects = req.param('subjects');
    let classesIds = [], subjectIds = [];

    if(masteredClasses && Array.isArray(masteredClasses)){
      for(let i = 0; i < masteredClasses.length; i++){
          classesIds.push(parseInt(masteredClasses[i]));
      }

    }else{
      classesIds.push(parseInt(masteredClasses));
    }

    if(subjects && Array.isArray(subjects)){
      for(let i = 0; i < subjects.length; i++){
          subjectIds.push(parseInt(subjects[i]));
      }

    }else if(subjects){
      subjectIds.push(parseInt(subjects));
    }
    let user = {
      username: req.param("username"),
      firstName: req.param("firstname"),
      lastName: req.param("lastName"),
      email: req.param("email"),
      mobileNumber: req.param("phone"),
      birthDate: req.param("birth"),
      belongingTo: req.param("school_id"),
      type: req.param('user-role'),
      classes: classesIds,
      subjects: subjectIds,
      address : req.param('address'),
      homeNumber : req.param('homeNumber'),
      profileImage : req.param('profileImage')
    };
    if(req.param('user-role') === CONSTANTS.userTypes.student){

      user.enrolledClass = Array.isArray(req.param('classes'))? parseInt(req.param('classes')[0]) : parseInt(req.param('classes'));
      var names = user.lastName.split(" ");
      const parentFirstName = names[0];
      const parentFullLastName = user.lastName.substring(names[0].length + 1, user.lastName.length);
      const parentInfo = {
        'username' : req.param('parentUsername'),
        'mobileNumber' : req.param('parentnumber'),
        'email' : req.param("parentemail"),
        'school' : req.param("school_id"),
        'firstName' : parentFirstName,
        'lastName' : parentFullLastName
      };

      UsersService.addStudent(parentInfo, user).then((results)=>{
        const successMessageStudent = UtilService.parseMessage(MESSAGES.users.userCreated.message, [results.student.username, results.student.password]);
        req.flash('message', successMessageStudent);
        let successMessageParent;
        if(results.parent.status === CONSTANTS.statuses.created){
          successMessageParent = UtilService.parseMessage(MESSAGES.users.attachedStudent.message, [results.student.username, results.parent.object.username]);

        }else if(results.parent.status === CONSTANTS.statuses.new){
          successMessageParent = UtilService.parseMessage(MESSAGES.users.userCreated.message, [results.parent.object.username, results.parent.object.password]);
        }

        req.flash('message', successMessageParent);

        const mailSuccess = UtilService.parseMessage(MESSAGES.users.mailSentSuccess.message, [results.parent.object.username]);
        req.flash('message', mailSuccess);
        req.flash('message', UtilService.parseMessage(MESSAGES.users.mailSentSuccess.message, [results.student.username]));



        var resultObjectArr = [];

        DivisionService.getSchoolDivsionsDeepNested(req.params.school_id).then((deepExpandedDivison)=>{
          return res.view('users/users_new', {
              "divisions": deepExpandedDivison
          });
        },(err)=>{
          Utils.sendError(res, err);
        });

      }, (err)=>{
        Utils.sendError(res, err);
      });

    }else if(user.type === CONSTANTS.userTypes.admin){

      UsersService.createMaster(user, masteredClasses).then((result)=>{
        const mailSuccess = UtilService.parseMessage(MESSAGES.users.mailSentSuccess.message, [result.user.username]);
        const successMessageUser = UtilService.parseMessage(MESSAGES.users.userCreated.message, [result.user.username, result.user.password]);

        req.flash('message', successMessageUser);
        req.flash('message', mailSuccess);

        DivisionService.getSchoolDivsionsDeepNested(req.params.school_id).then((deepExpandedDivison)=>{
          return res.view('users/users_new', {
              "divisions": deepExpandedDivison
          });
        },(err)=>{
          Utils.sendError(res, err);
        });
      },(err)=>{
        Utils.sendError(res, err);
      });
    }else{
      UsersService.createUser(user).then((result)=>{
        const mailSuccess = UtilService.parseMessage(MESSAGES.users.mailSentSuccess.message, [result.user.username]);
        const successMessageUser = UtilService.parseMessage(MESSAGES.users.userCreated.message, [result.user.username, result.user.password]);
        const mailType = CONSTANTS.emailMessageType.accCreated.name;

        MailService.sendEmail(mailType, result.user.email, {'user' : result.user, schoolId: result.user.belongingTo}).then((result)=>{
          console.log(result);
        }, (err)=>{
          console.log(err);
        });


        req.flash('message', successMessageUser);
        req.flash('message', mailSuccess);

        DivisionService.getSchoolDivsionsDeepNested(req.params.school_id).then((deepExpandedDivison)=>{
          return res.view('users/users_new', {
              "divisions": deepExpandedDivison
          });
        },(err)=>{
          Utils.sendError(res, err);
        });
      }, (err)=>{
        Utils.sendError(res, err);
      });

    }
  },
  updateUsers: function (req, res) {

    let masterClasses = req.param('classes');
    let subjects = req.param('subjects');
    //masters can' edit his classes
    if(req.user.id === req.param('id')){
      masterClasses = [];
      subjects = [];
    }

    let passwordObj = {
      password : req.param('password'),
      newPassword : req.param('newPassword'),
      confirmPassword : req.param('confirmPassword')
    };

    let classesIds = [], subjectIds = [];
    if(masterClasses && Array.isArray(masterClasses)){
      for(let i = 0; i < masterClasses.length; i++){
          classesIds.push(parseInt(masterClasses[i]));
      }

    }else if(masterClasses){
      classesIds.push(parseInt(masterClasses));
    }

    if(subjects && Array.isArray(subjects)){
      for(let i = 0; i < subjects.length; i++){
          subjectIds.push(parseInt(subjects[i]));
      }

    }else if(subjects){
      subjectIds.push(parseInt(subjects));
    }

    let user = {
      id : req.param('id'),
      username: req.param("username"),
      firstName: req.param("firstname"),
      lastName: req.param("lastname"),
      email: req.param("email"),
      mobileNumber: req.param("phone"),
      birthDate: req.param("birth"),
      classes: classesIds,
      subjects: subjectIds,
      type : req.param('type'),
      belongingTo : req.param('school_id'),
      enrolledClass : classesIds[0],
      address : req.param('address'),
      homeNumber : req.param('homeNumber')
    };

    if(req.user.id === parseInt(req.param('id'))){
      delete user.subjects;
      delete user.masterClasses;
    }

    user.id = parseInt(user.id);
    if(req.user.id == user.id){
      if(passwordObj.newPassword && passwordObj.password && passwordObj.confirmPassword && passwordObj.newPassword != passwordObj.confirmPassword){
        req.flash('error', MESSAGES.updatePassword.confirmPassword.message);
        req.query.update = true;
        return renderUserUpdate(req, res);
      }
      if(passwordObj.newPassword && passwordObj.password && passwordObj.confirmPassword && !UtilService.matchedPlainHashPassword(passwordObj.password, req.user.password)){
        req.flash('error', MESSAGES.updatePassword.error.message);
        req.query.update = true;
        return renderUserUpdate(req, res);
      }
    }

    if(passwordObj.newPassword || passwordObj.confirmPassword){
      if(passwordObj.newPassword != passwordObj.confirmPassword){
        req.flash('error', MESSAGES.updatePassword.confirmPassword.message);
        req.query.update = true;
        return renderUserUpdate(req, res);
      }
      user.password = passwordObj.newPassword;
    }

    if(user.type === CONSTANTS.userTypes.student){
      var names = user.lastName.split(" ");
      const parentFirstName = names[0];
      const parentFullLastName = user.lastName.substring(names[0].length + 1, user.lastName.length);
      const parentInfo = {
        'username' : req.param('parentUsername'),
        'mobileNumber' : req.param('parentnumber'),
        'email' : req.param("parentemail"),
        'school' : req.param("school_id"),
        'firstName' : parentFirstName,
        'lastName' : parentFullLastName
      };

      UsersService.updateStudent(parentInfo, user).then((student)=>{
        req.flash('message', MESSAGES.success.update.message);
        if(req.user.id == user.id){
          req.query.update = true;
          return renderUserUpdate(req, res);
        }
        res.redirect("/schools/" + student.belongingTo + "/users?type=student");
      }, (err)=>{
        req.flash('error', MESSAGES.fail[err]);
        res.redirect("/schools/" + user.belongingTo + "/users?type=student");
      });
    }else{

    Users.findOne(req.param('id')).populate('masterClasses').exec((err, userObj)=>{
      if(!err){
        const validationResult = ValidationService.validateEditUser(req, userObj);
        if(validationResult){
          req.flash('message', MESSAGES.businessValidation.updateMaster.message);
          res.redirect("/schools/" + userObj.belongingTo + "/users?type=admin&type=teacher");
        }else{
          let enrolledClass = null;

          if(userObj && userObj.type ===CONSTANTS.userTypes.student){
              enrolledClass = req.param('classes');
              user.enrolledClass = enrolledClass;
          }

          if(classesIds.length > 0 && user.type === CONSTANTS.userTypes.admin){
            user.masterClasses = classesIds;
          }else{
            delete user.classes;
          }
          if(user.type === CONSTANTS.userTypes.teacher){
           delete user.classes;
          }
          Users.update(req.param('id'), user).exec(function createCB(err, updated) {
            console.log("Error");
            console.log(err);
            console.log("User origin");
            console.log(user);
          if (err) return res.serverError(err);
          if(req.user.id == user.id){
            req.flash('message', MESSAGES.success.update.message);
            req.query.update = true;
            return renderUserUpdate(req, res);
          }

          if(userObj.type ===CONSTANTS.userTypes.admin || userObj.type===CONSTANTS.userTypes.teacher)
            return res.redirect("/schools/" + userObj.belongingTo + "/users?type=admin&type=teacher");
          else{
            return res.redirect("/schools/" + userObj.belongingTo + "/users?type=student");
          }
        });

        }
      }else{
        Utils.sendError(res, err);
      }
    });
    }
  },
  renderUsersNew: function (req, res) {

    DivisionService.getSchoolDivsionsDeepNested(req.params.school_id).then((deepExpandedDivison)=>{
      return res.view('users/users_new', {
          "divisions": deepExpandedDivison
      });
    },(err)=>{
      Utils.sendError(res, err);
    });
  },
  renderUsersSubject: function (req, res) {
    if (req.user.type === CONSTANTS.userTypes.student || req.user.type === CONSTANTS.userTypes.parent) {
      res.redirect("/schools/" + req.param('school_id') + "/classes/" + req.param('class_id') + "/subjects/" + req.param('subject_id') + "/users/" + req.user.id + "/menu");
    }else{

      let promises = [];
      promises.push(SubjectService.getSubjectById(req.param('subject_id')));
      promises.push(ClassService.getClassById(req.param('class_id')));
      Promise.all(promises).then((results)=>{


        res.view('subjects/users_list', {
          users_list: results[1].students,
          subject : results[0],
          'classObj' :  results[1]
        });
      },(err)=>{
        return res.serverError(err);
      });
    }
  },

  renderUserUpdate: function (req, res) {

        const update = req.query.update;
        Users.findOne(req.param('user_id')).populate('parent').exec(function (err, found) {
          if(!err && !found){
            req.flash('message' , 'User is not exist');
            res.redirect('/');
          }else if(!err){
            var dateTemp=dateFormat(found.birthDate,"mm-dd-yyyy");
            found.birthDate=dateTemp;
            Divisions.find({
                    belongingTo: found.belongingTo
                }).populate('classes')
                .exec(function(err, divisions2) {


                  var getClassesPromises = [];
                  for(var iDivision = 0; iDivision < divisions2.length; iDivision++){
                    var classesIds = [];
                    for(var iClass = 0; divisions2 && iClass < divisions2[iDivision].classes.length; iClass++){
                      classesIds.push(divisions2[iDivision].classes[iClass].id);
                    }
                    getClassesPromises.push(getClassExpanded(divisions2[iDivision],classesIds ));
                  }
                  Promise.all(getClassesPromises).then((divisionsExpanded)=>{
                    var classes = [];
                    for(var iDivision = 0; iDivision < divisionsExpanded.length; iDivision++){


                      classes = classes.concat(divisionsExpanded[iDivision].classes);
                    }
                    req.flash('Getting data');
                    return res.view('users/user_update', {
                        "divisions": divisionsExpanded,

                         user_u: found,
                         classes_list: classes,
                         update : update
                    });
                  },(err)=>{
                    console.log(err);
                  });


                });
          }else{
            req.flash('message' , 'Bad request');
            res.redirect('/');
          }
        });


  },


  renderMenu: function (req, res) {

    Subjects.findOneById(req.param('subject_id')).exec(function (err, subject ) {
        res.view("users/user_menu",{subject:subject});

    });

  },

  apigrades: function (req, res) {
    Grades.find({
      belongingTo: req.param('user_id'),
      inSubject: req.param('subject_id')
    }).exec(function (err, grades) {
      if (err) return res.json(err);
      else res.json(grades);
    })
  },

  apihomework: function (req, res) {
    Homework.find({
      'subject': req.param('subject_id')
    }).exec(function (err, homework) {
      if (err) return res.send(err);
      else res.json(homework);
    })
  },
  apiattendance: function (req, res) {
    Attendance.find({
      belongingTo: req.param('user_id'),
      inSubject: req.param('subject_id')
    }).exec(function (err, attendance) {
      if (err) return res.json(err);
      else res.json(attendance);
    })
  },
  apisubjects: function (req, res) {
    Subjects.findByBelongingTo(req.param("class_id")).exec(function (err, subjects_f) {
      if (err) return res.json(err);
      else return res.json(subjects_f);
    });
  },
  apiuserdata: function (req, res) {
    Users.findOneById(req.param("user_id")).exec(function (err, user_f) {
      Classes.findOneById(user_f.classes[0]).exec(function (err, class_f) {
        if (err) return res.json(err);
        else return res.json({ user: user_f, class_found: class_f });
      });
    })
  },
  apiclasses: function (req, res) {
    Classes.findOneById(req.param("class_id")).exec(function (err, class_f) {
      if (err) return res.json(err);
      else return res.json(class_f);
    });
  },
  apischool: function (req, res) {
    Schools.findOneById(req.param("school_id")).exec(function (err, school_f) {
      if (err) return res.json(err);
      else return res.json(school_f);
    });
  },
  updateStudent : function(req, res){
    var userObj = req.body;

    Users.update(userObj.id, {
       username: userObj.username,
       password: userObj.password,
       firstName: userObj.firstName,
       lastName: userObj.lastName,
       email: userObj.email,
       mobileNumber: userObj.mobileNumber,
       parentEmail: userObj.parentEmail,
       parentNumber: userObj.parentNumber,
       profileImage : userObj.profileImage
    }).exec(function createCB(err, updated) {
      if (err) return res.json(err);
      else return res.json(updated);
    });
  },
  apiupdate: function (req, res) {

    Users.update(req.param('user_id'), {
       username: req.param("username"),
       password: req.param("password"),
       firstName: req.param("firstName"),
       lastName: req.param("lastName"),
       email: req.param("email"),
       mobileNumber: req.param("mobileNumber"),
       parentEmail: req.param("parentEmail"),
       parentNumber: req.param("parentNumber"),
       classes: req.param('classes'),
       subjects :req.param('subjects')
    }).exec(function createCB(err, updated) {
      if (err) return res.json(err);
      else return res.json("ok");
    });
  },
  apiannouncement: function (req, res) {
    Announcements.find().exec(function (err, announcement_f) {
      if (err) return res.json(err);
      else return res.json({ success: 1, result: announcement_f });
    });
  },
  search: function (req, res) {
    if (req.param('school_id') == "undefined") return res.redirect('/');
    Users.find({ /*belongingTo:req.param('school_id'),*/
      username: {
        'contains': req.param('name')
      },
      type: req.param('type')
    }).exec(function (err, users) {
      if (err) return res.serverError(err);
      else {
        return res.view('users/users', {
          users_list: users
        });
      }
    });
  },
  removeUser: function (req, res) {
    let type = req.query.type;
    let schoolId = req.query.school_id;
    // this should also remove users, Subjects and anything associated with it
    Users.destroy(req.param('user_id')).exec(function (err) {
      if (err) return res.send(err);
      else if(type === CONSTANTS.userTypes.student){
        res.redirect("/schools/" + schoolId + "/users?type=" +CONSTANTS.userTypes.student);
      }else{
        res.redirect("/schools/" + schoolId + "/users?type=" +CONSTANTS.userTypes.teacher +'&type='+ CONSTANTS.userTypes.admin);
      }
    });
  },
  dataview: function (req, res) {
    res.view('admin/data_view')
  },
  getUsers : function(req, res){
    const schoolId = req.query.schoolId;
    const type = req.query.type;
    const email = req.query.email;
    const username = req.query.username;
    const enrolledClass = req.query.enrolledclass;
    let query = {
      belongingTo : schoolId,
      type : type
    };
    const userId = req.query.userId;

    if(schoolId && type){
      UsersService.getUsers(query).then((users)=>{
        res.send(users);
      },(err)=>{
        Utils.sendError(res, err);
      });
    }else if(userId && type === CONSTANTS.userTypes.admin ){
      Users.findOneById(masterId).populate('masterClasses').exec((err, userObj)=>{
        let classesIds = [];
        for(let i = 0; i < userObj.masterClasses.length; i++){
          classesIds.push(userObj.masterClasses[i].id);
        }
        Users.find({enrolledClass : classesIds}).exec((err, users)=>{
          res.send(users);
        });
      });
    }else if(userId && type === CONSTANTS.userTypes.teacher ){
      Users.findOneById(userId).exec((err, userObj)=>{
        Subjects.find({id : userObj.subjects}).exec((err, subjects)=>{
          let classesIds = [];
          for(let i = 0; i < subjects.length; i++){
            classesIds.push(subjects[i].class);
          }
          Users.find({enrolledClass : classesIds}).exec((err, students)=>{
            res.send(students);
          })
        });
      })
    }else if(email){
      Users.find({email : email}).populate('children').exec((err, users)=>{
        if(!err){
          return res.send(users);
        }
        Utils.sendError(res, err);
      });
    }else if(username){
      Users.find({username : username}).populate('children').exec((err, users)=>{
        if(!err){
          return res.send(users);
        }
        Utils.sendError(res, err);
      });
    }else if(enrolledClass){
      Users.find({enrolledClass : enrolledClass}).exec((err, users)=>{
        if(!err){
          return res.send(users);
        }
        Utils.sendError(res, err);
      });
    }
  },
  patchInsertUsers : function(req, res){

      let dataArr = [];
      fs = require('fs')


  // res.view('users/patchInsertResult.ejs', {results : []});
  // return;
    req.file('file').upload({
        dirname: require('path').resolve(sails.config.appPath, '../uploads'),
        maxBytes: 10000000
      }, function(err, files) {

        if (err) {
          return console.log(err);
        }

        fs.readFile(files[0].fd, 'utf8', function (err,data) {
          if (err) {
            return console.log(err);
          }
          console.log(data);
        });
        csv({noheader:false})
        .fromFile(files[0].fd)
        .on('json',(csvRow)=>{ // this func will be called 3 times

            dataArr.push(csvRow);
        })
        .on('done',()=>{
            //parsing finished
            UsersService.insertPatchStudents(dataArr, req.session.school.id).then((results)=>{
              // res.setHeader('Content-disposition', 'attachment; filename=students.csv');
              // res.set({ 'content-type': 'charset=utf-8' })
              // res.send(new Buffer(results));
              // res.view('users/patchInsertResult.ejs', {results : results});
              res.view('users/patchInsertResult.ejs', {results : results});
              // res.send(results);
            }, (err)=>{
              Utils.sendError(res, err);
            });
        })


    });
  },
  renderPatchInsert : function(req, res){
    res.view("users/patchInsert");
  }
};


var getClassExpanded = function(division, classIds){
  return new Promise((resolve, reject)=>{
    Classes.find({id : classIds}).populate('subjects').exec((err, classes)=>{
      if(!err){
        division = division.toJSON();
        division.classes = classes;


        resolve(division);
      }else{
        reject(err);
      }
    });
  });
};

function renderUserUpdate  (req, res) {

      const update = req.query.update;
      Users.findOne(req.param('user_id')).populate('parent').exec(function (err, found) {
        if(!err && !found){
          req.flash('message' , 'User is not exist');
          res.redirect('/');
        }else if(!err){
          var dateTemp=dateFormat(found.birthDate,"mm-dd-yyyy");
          found.birthDate=dateTemp;
          Divisions.find({
                  belongingTo: found.belongingTo
              }).populate('classes')
              .exec(function(err, divisions2) {


                var getClassesPromises = [];
                for(var iDivision = 0; iDivision < divisions2.length; iDivision++){
                  var classesIds = [];
                  for(var iClass = 0; divisions2 && iClass < divisions2[iDivision].classes.length; iClass++){
                    classesIds.push(divisions2[iDivision].classes[iClass].id);
                  }
                  getClassesPromises.push(getClassExpanded(divisions2[iDivision],classesIds ));
                }
                Promise.all(getClassesPromises).then((divisionsExpanded)=>{
                  var classes = [];
                  for(var iDivision = 0; iDivision < divisionsExpanded.length; iDivision++){


                    classes = classes.concat(divisionsExpanded[iDivision].classes);
                  }
                  req.flash('Getting data');
                  return res.view('users/user_update', {
                      "divisions": divisionsExpanded,

                       user_u: found,
                       classes_list: classes,
                       update : update
                  });
                },(err)=>{
                  console.log(err);
                });


              });
        }else{
          req.flash('message' , 'Bad request');
          res.redirect('/');
        }
      });


}
