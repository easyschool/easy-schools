const CONSTANTS = sails.config.constants;


module.exports = {
  attachFile : function (req, res) {
    console.log(process.env.NODE_ENV);
    console.log("divisions :" + sails.config.appPath);
    req.file('file').upload({
        dirname: require('path').resolve(sails.config.appPath, '../uploads'),
        maxBytes: 10000000
      }, function(err, files) {
       if (err) res.send(err);
        else {
          let path = CONSTANTS.baseURL[process.env.NODE_ENV] + ':8000/' + files[0].fd.split('uploads/')[1];
          res.send({path:path});
        }
    });
  }
};
