/**
 * SchoolLevelController
 *
 * @description :: Server-side logic for managing Schoollevels
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	renderSchoolLevel: function (req, res) {

		Divisions.findByBelongingTo(req.param('school_id')).populate('classes').exec(function (err, divisions) {

			var classes = [];
			var classesTemp = req.user.classes;
			if(classesTemp){
					for(var i = 0; i < classesTemp.length; i++){
						classes.push(parseInt(classesTemp[i]));
					}
			}

			Classes.find({id : classes}).exec(function (err, classes) {
				res.view('years/years', {
					classes: classes,
					divisions_list: divisions
				});
			});
		});
	},
// 	renderSchoolLevel: function (req, res) {
// 		console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
// 		console.log(req.user);
// 		Classes.find({}).exec(function (err, classes) {
// 			var classes_belonging = []
// 			//console.log("req.user"+req.user);
//
//
// 			for (i = 0; i < classes.length; i++) {
// 			var class_idd = parseInt(classes[i].id);
// 			for (j = 0; j < classes.length; j++) {
// 				if (classes[j].id === class_idd)
// 					{
// 					classes_belonging.push( classes[j]  );
// 				}
// 			}
// }
// 			Divisions.findByBelongingTo(req.param('school_id')).exec(function (err, divisions) {
// 				res.view('years/years', {
// 					classes_list: classes,
// 					classes_list2: classes_belonging,
// 					divisions_list: divisions
// 				});
// 			})
// 		});
// 	},
	renderSchoolLevelNew: function (req, res) {
		res.view('years/years_new');
	},

	createDivision: function (req, res) {
		Divisions.create({
			name: req.param("name"),
			belongingTo: req.param('school_id'),
			school : parseInt(req.param('school_id'))
		}).exec(function createCB(err, created) {
			if (err) return res.serverError(err);
			else return res.redirect("/schools/" + req.param('school_id') + "/years");
		});
	}
};

var getClass = function(division, classIds){
  return new Promise((resolve, reject)=>{
    Classes.find({id : classIds}).exec((err, classes)=>{
      if(!err){
        division = division.toJSON();
        division.classes = classes;
        resolve(division);
      }else{
        reject(err);
      }
    });
  });
};
