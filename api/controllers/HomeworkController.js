/**
 * HomeworkController
 *
 * @description :: Server-side logic for managing homework
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  renderHomeworks: function(req, res) {
    // Homework.find({subject : parseInt(req.param('subject_id'))}).exec((err, hws)=>{
      Subjects.findOne({id : parseInt(req.param('subject_id'))}).exec((err, subjectObj)=>{
        HomeworkService.getClassHomeworks('asd', req.param('subject_id')).then((homeworks)=>{
          res.view('homeworks/homeworks', {
            hw_list: homeworks,
            subject : subjectObj
          });
        }, (err)=>{
          Utils.sendError(res, err);
        });

      });
    // });

  },
  renderHomeworksNew: function(req, res) {
    res.view('homeworks/homeworks_new');
  },
  createHomework: function(req, res) {
    const homework = {
      name: req.param("name"),
      dueDate: req.param("dueDate"),
      subject: req.param("subject_id"),
      questionAttache : req.param('questionAttache'),
      description: req.param("description")
    };

    console.log(homework);
    Homework.create(homework).exec(function createCB(err, created) {
      if (err) return res.serverError(err);
      else return res.redirect("/schools/" + req.param('school_id') + "/classes/" + req.param('class_id') + "/subjects/" + req.param('subject_id') + "/hw");
    });
  },
  updateHomework: function(req, res) {
    let homework = {
      name: req.param("name"),
      dueDate: req.param("dueDate"),
      subject: req.param("subject_id"),
      description: req.param("description")
    };
    if(req.param('question-attache')){
      homework['question-attache'] = req.param('question-attache');
    }
    Homework.update({id : req.param('hwId')},homework).exec(function createCB(err, created) {
      if (err) return res.serverError(err);
      else return res.redirect("/schools/" + req.param('school_id') + "/classes/" + req.param('class_id') + "/subjects/" + req.param('subject_id') + "/hw");
    });
  },
  deleteHomework : function(req, res){
    Homework.destroy(req.params.hwId).exec((err, result)=>{
      if(!err){
        res.send(result);
      }else{
        Utils.sendError(res, err);
      }
    })
  },
  renderStudentHomework : function(req, res){
    const subjectId = req.params.subject_id;
    const target = req.query.target;
    HomeworkService.getStudentHomework(req.user.id,subjectId).then((homeworks)=>{
      if(target){
        return res.send(homeworks);
      }
      res.view('homeworks/homework_student', {
        homeworks : homeworks
      });
    }, (err)=>{
      Utils.sendError(res, err);
    });

  },
  submitAnswer : function(req, res){
    const answer = req.body;
    const target = req.query.target;
    if(req.body.answerId === '-1' || req.body.answerId === -1){
      Studenthomework.create(answer).exec((err, result)=>{
        if(!target){
          res.redirect('/schools/' + req.params.school_id + '/classes/' + req.params.class_id + '/subjects/' + req.params.subject_id + '/users/' + req.params.user_id + '/hw');
        }else{
          res.send({'code': 'success', 'message' : 'answer submitted successfully'});
        }

      });
    }else{
      Studenthomework.update({id : req.body.answerId}, req.body).exec((err, result)=>{
        if(!target){
            res.redirect('/schools/' + req.params.school_id + '/classes/' + req.params.class_id + '/subjects/' + req.params.subject_id + '/users/' + req.params.user_id + '/hw');
        }else{
          res.send({'code': 'success', 'message' : 'answer submitted successfully'});
        }
      });
    }

  }


};
