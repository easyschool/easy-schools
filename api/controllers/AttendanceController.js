 /**
 * AttendanceController
 *
 * @description :: Server-side logic for managing attendances
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
 var moment = require('moment');
module.exports = {
  renderAttendance: function (req, res) {
    Attendance.find({
      belongingTo: req.param('user_id'),
      inSubject: req.param('subject_id')
    }).exec(function (err, attendance) {

      Subjects.findOne({id : parseInt(req.param('subject_id'))}).exec((err, subjectObj)=>{

        res.view('attendance/attendance', {
          attendance_list: attendance,
          moment:moment,
          subject : subjectObj
        });
      });
    });
  },
  renderAttendanceNew: function (req, res) {
    res.view('attendance/attendance_new')
  },
  createAttendance: function (req, res) {
    Attendance.create({
      date: req.param("date"),
      attended: req.param("attended"),
      belongingTo: req.param("user_id"),
      inSubject: req.param("subject_id"),
      subject: parseInt(req.param("subject_id")),
      student :  parseInt(req.param("user_id"))
    }).exec(function createCB(err, created) {

      if (err) return res.serverError(err);
      else return res.redirect("/schools/" + req.param('school_id') + "/classes/" + req.param('class_id') + "/subjects/" + req.param('subject_id') + "/users/" + req.param('user_id') + "/attend");
    });
  },
  RecordAttend: function (req, res) {

    Attendance.find({subject: req.param("subject_id"), date : req.param("date")}).exec((err, attends)=>{
      if(err){
        return Utils.sendError(res, err);
      }

      if(attends && attends.length > 0){
        return res.redirect("/schools/" + req.param('school_id') + "/classes/" + req.param('class_id') + "/subjects/" + req.param('subject_id') + "/attend/new?code=duplicate_date_attend&type=alert-danger");
      }else{
        Subjects.findOneById(req.param("subject_id")).populate('students').exec((err, subject)=>{

          if(subject.optional){
            createAttends(subject.students, req.param("date"), req, res);
          }else{
            Users.find({enrolledClass : req.param('class_id')}).exec(function (err, users_f) {
              createAttends(users_f, req.param("date"), req, res);
            });
          }
          return res.redirect("/schools/" + req.param('school_id') + "/classes/" + req.param('class_id') + "/subjects/" + req.param('subject_id') + "/users");
        });
      }
    });
  },
  apiattendance: function (req, res) {
    Attendance.find({
      belongingTo: req.param('user_id'),
      inSubject: req.param('subject_id'),
      attended: 'false'
    }).exec(function (err, attendance) {
      if (err) return res.json(err);
      else res.json(attendance);
    });
  },
  renderRecordAttend: function (req, res) {
    Users.find({enrolledClass : req.param('class_id')}).exec((err, students)=>{
      if(err){
        return Utils.sendError(res, err);
      }

      var dates = {};
      Attendance.find({
        attended: true,
        inSubject: req.param('subject_id')
      }).exec(function (err, attends) {
        attends.forEach(function (attend) {
          if (dates[attend.date]) dates[attend.date].push(attend.belongingTo)
          else dates[attend.date] = [attend.belongingTo];
        });

        let promises = [];
        promises.push(SubjectService.getSubjectById(req.param('subject_id')));
        promises.push(ClassService.getClassById(req.param('class_id')));
        Promise.all(promises).then((results)=>{


          res.view('attendance/attendances_new', {
            students_list: students,
            recorded_dates: dates,
            subject : results[0],
            'classObj' :  results[1]
          });
        },(err)=>{
          return res.serverError(err);
        });
      });
    })

  },
  getStudentAttendance : function (req, res) {

    const showDay = req.query.showDay;
    let subject_id = req.param('subject_id');

    if(showDay === 'true' || showDay === true ){
      let query = {
          student: req.param('user_id'),
          inSubject: req.param('subject_id'),
          date : req.query.date
      }
      Attendance.find({
        student: req.param('user_id'),
        inSubject: req.param('subject_id'),
        date : req.query.date
      }).exec(function (err, attendances) {

      if(err){
        return Utils.sendError(res, err);
      }
      res.send(attendances);

    });
    }else{
      let date = new Date(req.query.date);
      let startDate = new Date((date.getMonth()+1) + "/1/" + date.getFullYear());
      let endDate = new Date((date.getMonth() + 2) + "/1/" + date.getFullYear());
      const query = "SELECT * "+
                    "FROM easyschools.attendance  attend "+
                    "where attend.student = " + req.param('user_id') +  " and date > '" + UtilService.getFormattedDate(startDate) + "' and date < '" + UtilService.getFormattedDate(endDate) + "' and subject=" + subject_id;
      Attendance.query(query, '', function(err, rawResult) {
        if(err){
          Utils.sendError(res, err);
        }
        res.send(rawResult);
      });

    }
  },
  updateAttendance : function(req, res){
    attendId = req.params.id;
    attendType = req.query.type;
    attendObject = {
      attended : attendType === 'true' ? true : false
    };

    Attendance.update({id : attendId}, attendObject).exec((err, attendObjectUpdated)=>{
      if(!err){
        return res.send(attendObjectUpdated);
      }
      Utils.sendError(res, err);
    });
  },
  getAttendanceByDate: function(req,res){

    const subject_id = req.query.subject_id;
    const showDay = req.query.showDay;
    if(showDay === 'true' || showDay === true ){
      Attendance.find({
      date: req.query.date,
      inSubject: subject_id
      }).exec((err, attends)=>{
        studentIds = [];
        attends.forEach((attend)=>{
          studentIds.push(attend.belongingTo);

        });
        Users.find({"type": "student"}).where({ id: studentIds }).exec((err, students)=>{
          res.send({attends: attends, students: students});
        });

    });
  }else{
    let date = new Date(req.query.date);
    let startDate = new Date((date.getMonth()+1) + "/1/" + date.getFullYear());
    let endDate = new Date((date.getMonth() + 2) + "/1/" + date.getFullYear());
    const query = "SELECT "+
                  "users.id student, attend.id attendId, attend.date, attend.attended, users.firstName, users.lastName, users.profileImage "+
                  "FROM easyschools.attendance  attend, easyschools.users users "+
                  "where users.id = attend.student  and date > '" + UtilService.getFormattedDate(startDate) + "' and date < '" + UtilService.getFormattedDate(endDate) + "' and subject=" + subject_id;
    Attendance.query(query, '', function(err, rawResult) {
      if(err){
        Utils.sendError(res, err);
      }
      let studentResult = {};
      let student;
      for (var i = 0; i < rawResult.length; i++) {
        if(!studentResult[rawResult[i].student]){
          student = {
            id : rawResult[i].student,
            firstName : rawResult[i].firstName,
            lastName : rawResult[i].lastName,
            profileImage : rawResult[i].profileImage
          }
          studentResult[rawResult[i].student] = { student : student};
          studentResult[rawResult[i].student].attended = 0;
          studentResult[rawResult[i].student].notattended = 0;
        }

        if(rawResult[i].attended){
          studentResult[rawResult[i].student].attended++;
        }else{
          studentResult[rawResult[i].student].notattended++;
        }

      }
      res.send(UtilService.convertJsonIntoArrayValues(studentResult));
      console.log(studentResult);

    });

    // Attendance.find({date : {'>' : startDate, '<' : endDate}}).exec((err, attends)=>{
    //   if(err){
    //     return Utils.sendError(res, err);
    //   }
    //   res.send(attends);
    // });
  }
  },
  getAttendances: function(req,res){

    Attendance.find({
      belongingTo: req.param('user_id'),
      inSubject: req.param('subject_id'),
      attended : false
    }).exec(function (err, attendances  ) {
      if (err) return res.json(err);
      else res.json(attendances);
    });
  },

  viewattendance:function(req,res){

      var subject = req.param("subject_id");

      let promises = [];
      promises.push(SubjectService.getSubjectById(req.param('subject_id')));
      promises.push(ClassService.getClassById(req.param('class_id')));
      Promise.all(promises).then((results)=>{


        // res.view('attendance/attendance_view', {
        //   students_list: students,
        //   recorded_dates: dates,
        //   subject : results[0],
        //   'classObj' :  results[1],
        //   subjectid:subject
        // });

        return res.view('attendance/attendance_view',{
          subject : results[0],
          classObj :  results[1],
           subjectid:subject
        });
      },(err)=>{
        return res.serverError(err);
      });
    //  return res.view('attendance/attendance_view',{
    //     subjectid:subject
    //  });


  }

};

function createAttends(students, date, req, res){
  var student_ids = [];
  // const date = req.param("date");

  students.forEach(function (user) {
    student_ids.push(user.id)
  });
  if (req.param("students"))
    var attended_students = req.param("students").map(function (student) {

       return parseInt(student, 10)
    });
  else
    {

      var attended_students = [];

    }
  student_ids.forEach(function (studentId) {
    var attended = attended_students.indexOf(studentId) >= 0;

    Attendance.create({
      date: req.param("date"),
      belongingTo: studentId,
      inSubject: req.param("subject_id"),
      attended:  attended,
      subject: parseInt(req.param("subject_id")),
      student :  studentId
    }).exec(function (err, created) {
      if (err) return res.serverError(err);
    });
  });
}
