/**
 * GradesController
 *
 * @description :: Server-side logic for managing grades
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const Promise = require('promise');
module.exports = {
  renderGrades: function(req, res) {
    Grades.find({
      belongingTo: req.param('user_id'),
      inSubject: req.param('subject_id')
    }).exec(function(err, grades) {
      Subjects.findOne({id : parseInt(req.param('subject_id'))}).exec((err, subjectObj)=>{
            Classes.findOne({id:parseInt(req.param('class_id'))}).exec((err , classObj)=>{

                   res.view('grades/grades', {
                      grades_list: grades,
                      subject : subjectObj,
                      classobj:classObj
                    });
            });

      });
    });
  },
  renderGradesNew: function(req, res) {
    res.view('grades/grades_new')
  },
  renderRecordGrades: function(req,res) {
    Grades.find({
      inSubject: req.param("subject_id")
    }).exec(function(err, grades){
        Users.find({enrolledClass : req.param('class_id')}).exec(function(err, users_f) {
        var grade_by_title = {}
        grades.forEach(function(grade){
          if (grade_by_title[grade.name])
            grade_by_title[grade.name].push({
              id: grade.belongingTo,
              value: grade.value
            });
          else
            grade_by_title[grade.name] = [{
              id: grade.belongingTo,
              value: grade.value
            }];
        });
        res.view('grades/grades_multi_new', {students_list: users_f, grades: grade_by_title});
      });
    });
  },
  createGrad: function(req, res){
    const gradeObj = req.body;
    console.log(">>>>>>>>>>>>>>>>");
    console.log(gradeObj);
    GradeService.createGrade(gradeObj).then((newGrade)=>{
      res.json(newGrade);
    }, (err)=>{
      Utils.sendError(res, err);
    });
  },
  updateGrade: function(req, res){
    const gradeObj = req.body;
    GradeService.updateGrade(gradeObj).then((newGrade)=>{
      res.json(newGrade);
    }, (err)=>{
      Utils.sendError(res, err);
    });
  },
  addStudentGrades : function(req, res){
    const studentGrades = req.body;
    console.log("creating student grades");
    console.log(studentGrades);
    let promises = [];
    for(let i = 0; i < studentGrades.grades.length; i++){
          console.log("grade of each student");

          console.log(studentGrades.grades[i]);
      promises.push(GradeService.addStudentGrade(studentGrades.grades[i]));
    }
    Promise.all(promises).then((newStudentGrades)=>{
      res.json(newStudentGrades);
    },(err)=>{
      Utils.sendError(res, err);
    });
  },
  updateStudentGrades : function(req, res){

    const studentGrades = req.body;
    console.log("the updated grades list ");
    console.log(studentGrades.newgrades);
    let promises = [];
    for(let i = 0; i < studentGrades.newgrades.length; i++){
      promises.push(GradeService.updateStudentGrade(studentGrades.newgrades[i]));
    }
    Promise.all(promises).then((updatedStudentGrades)=>{
      res.json(updatedStudentGrades);
    },(err)=>{
      Utils.sendError(res, err);
    });

  },

  getGrades : function(req, res){
    // const subjectId = req.query.subjectId;
    // if(subjectId){
    //   Grades.find({subject : subjectId}).exec((err, grades)=>{
    //     if(!err){
    //       return res.json(grades);
    //     }
    //     Utils.sendError(res, err);
    //   });
    // }
    const classId = req.query.classId;
    const studentId = req.query.studentId;
    const subjectId = req.query.subjectId;
    
    GradeService.getStudentSubjectGradesExpanded(studentId, subjectId).then((results)=>{
      res.send(results);
    },(err)=>{
      Utils.sendError(res, err);
    });

  },
  RecordGrades: function(req,res) {
    req.param('students').forEach(function(student){
      Grades.create({
        name: req.param("title"),
        value: student.grade,
        belongingTo: student.id,
        inSubject: req.param("subject_id")
      }).exec(function(err,created){
        if (err) res.serverError(err);
      });
    });
    return res.redirect("/schools/" + req.param('school_id') + "/classes/" + req.param('class_id') + "/subjects/" + req.param('subject_id') + "/users");
  },
  createGrade: function(req, res) {
    Grades.create({
      name: req.param("name"),
      value: req.param("value"),
      belongingTo: req.param("user_id"),
      inSubject: req.param("subject_id")
    }).exec(function createCB(err, created) {
      if (err) return res.serverError(err);
      else return res.redirect("/schools/" + req.param('school_id') + "/classes/" + req.param('class_id') + "/subjects/" + req.param('subject_id') + "/users/" + req.param('user_id') + "/grades");
    });
  },
  viewGrades: function(req, res) {
    SubjectService.getSubjectById(req.params.subject_id).then((subject)=>{
      if(subject.optional){
        console.log(students);
        res.view('grades/grades_view',{
          students : subject.students,
          subject : subject
        });
      }else{
        Users.find({enrolledClass : req.params.class_id}).exec((err, students)=>{
          res.view('grades/grades_view',{
            students : students
          });
        });

      }
    }, (err)=>{
      Utils.sendError(res, err);
    });



  },
  deleteGrade : function(req, res){
    Grades.destroy(req.params.id).exec((err, result)=>{
      if(!err){
        res.send(result);
      }else{
        Utils.sendError(res, err);
      }
    })
  },
  getStudentSubjectGradesExpanded : function(req, res){
    const classId = req.query.classId;
    const studentId = req.query.studentId;
    const subjectId = req.query.subjectId;
    if(classId){
      GradeService.getClassSubjectGradesExpanded(classId, subjectId).then((results)=>{
        res.send(results);
      },(err)=>{
        Utils.sendError(res, err);
      });
    }else if(studentId){
      GradeService.getStudentSubjectGradesExpanded(studentId, subjectId).then((results)=>{
        res.send(results);
      },(err)=>{
        Utils.sendError(res, err);
      });
    }

  }
};
