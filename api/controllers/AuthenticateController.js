/**
 * UsersController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var passport = require("passport");
var dateFormat = require('dateformat');
const CONSTANTS = sails.config.constants;
const MESSAGES = sails.config.messages;
var Promise = require('promise');
module.exports = {

  login: function (req, res) {
    passport.authenticate('local', function (err, user, info) {
      // return res.redirect("/");
      if ((err) || (!user)) {
        req.flash("message", info.message);
        return res.redirect("/login")
      }
      req.logIn(user, function (err) {
        if (err) req.flash("message", err);

        Schools.findOneById(user.belongingTo).exec((err, school)=>{
          req.session.school = school;
          return res.redirect("/");
        });

      });
    })(req, res);
  },
  logout: function (req, res) {
    req.logout();
    res.redirect("/");
  },
  renderLogin: function (req, res) {
    var m = req.flash("message")[0]
    return res.view('login', {
      message: m
    });
  },
  apilogin: function (req, res) {
  var resultResponse={}
  var resultStatus=0;
 var user={};
 passport.authenticate('local', function (err, user, info) {
      if ((err) || (!user)) {
        resultResponse.resultStatus=resultStatus;
        resultResponse.user=info.message;
        return res.json(resultResponse);
      }
      req.logIn(user, function (err) {
        console.log("user :"+user.toString());
        if (err){
resultResponse.resultStatus=resultStatus;
        resultResponse.user=err;
          return res.json(resultResponse);
        }else{
       Users.findOneByUsername(user.username).exec(function (err2, found) {
      if (err){
resultResponse.resultStatus=resultStatus;
        resultResponse.user=err2;
        return res.json(resultResponse);
      }
resultResponse.resultStatus=1;
        resultResponse.user=found;
       return res.json(resultResponse);
     });
     }
      });

    })(req, res);
},
}
