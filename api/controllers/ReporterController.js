/**
 * SchoolLevelController
 *
 * @description :: Server-side logic for managing Schoollevels
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 var json2csv = require('json2csv');
const CONSTANTS = sails.config.constants;
module.exports = {
  logReportData : function(req, res){


    if(req.user.type !== CONSTANTS.userRoles.individuals.superuser){
      let object = req.body;
      // object.enterDate = new Date();
      object.user = req.user.id;
      object.school = req.session.school.id;
      object.type = req.user.type;
      LogReport.create(object).exec((err, record)=>{
        if(err){
          Utils.sendError(res, err);
        }
        res.send(record);
      });
    }else{
      res.send({'superuser' : "not supported"});
    }

  },

  updateReportData : function(req, res){
    if(req && req.user && req.user.type !== CONSTANTS.userRoles.individuals.superuser){
    LogReport.findOne({id : req.params.id}).exec((err, log)=>{
      if(log){
        const timeSpent = parseInt((new Date().getTime() - log.enterDate.getTime()));
        log.lastVisitDate = req.body.lastVisitDate;
        log.timeSpent = msToTime(timeSpent);
        LogReport.update({id : req.params.id}, log).exec((err, updateLog)=>{
          if(!err){
            return res.send(updateLog)
          }
          Utils.sendError(res, err);
        });


      }else{
        res.send('not supported')
      }

    });
  }else{
    res.send({'superuser' : "not supported"});
  }
  },

  genereateCSVReport : function(req, res){
    let schoolId;
    if(req.user.belongingTo && req.user.type === CONSTANTS.userRoles.individuals.admin){
      schoolId = req.user.belongingTo;
    }else if(req.user.type === CONSTANTS.userRoles.individuals.superuser){
      schoolId = req.query.schoolId;
    }else{
      return res.send('Not supported')
    }

    LogReport.find({school : schoolId, type : [CONSTANTS.userRoles.individuals.admin,CONSTANTS.userRoles.individuals.teacher]}).populate('user').exec((err, logs)=>{
      let logJson = logs.map((log)=>{
        let logTemp = {
          Page :log.page,
          Name : log.user.firstName + ' ' + log.user.lastName,
          Type : log.type,
          'Time Spent' : log.timeSpent,
          'Enter Date' : log.enterDate,
          'Last Visit Date' : log.lastVisitDate,
          'Username' : log.user.username
        };
        return logTemp;
      })

      const fields = ['Username', 'Name', 'Type', 'Page', 'Time Spent', 'Enter Date', 'Last Visit Date'];

      try {
        const result = json2csv({ data: logJson, fields: fields });
        const date = new Date();
        res.setHeader('Content-disposition', 'attachment; filename=sheet_' + date + '.csv');
        res.send(new Buffer(result));
      } catch (err) {
        // Errors are thrown for bad options, or if the data is empty and no fields are provided.
        // Be sure to provide fields if it is possible that your data array will be empty.

        res.error(err);
      }



    })
  }
};



function msToTime(duration) {
        var milliseconds = parseInt((duration%1000)/100)
            , seconds = parseInt((duration/1000)%60)
            , minutes = parseInt((duration/(1000*60))%60)
            , hours = parseInt((duration/(1000*60*60))%24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
    }
