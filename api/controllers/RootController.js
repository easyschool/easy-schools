/**
 * RootController
 *
 * @description :: Server-side logic for managing roots
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  renderRoot: function(req, res) {
    if (req.user) {
      if (req.user.type == "superuser")
        res.redirect('/schools');

      if (req.user.type == "admin")
        res.redirect('/schools/' + req.user.belongingTo);
      else if(req.user.type === "parent" || req.user.type === "student" || req.user.type === "teacher"){
        res.redirect('/schools/' + req.user.belongingTo + '/users/' + req.user.id + '/home?userType=' + req.user.type);
      }
    } else
      res.redirect('/schools');
  }
};
