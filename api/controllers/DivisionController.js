/**
 * DivisionsController
 *
 * @description :: Server-side logic for managing divisons
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const CONSTANTS = sails.config.constants;
module.exports = {
	renderDivsion: function (req, res) {
    const schoolId = parseInt(req.params.school_id);

    if(isNaN(schoolId)){
      Utils.redirectToRoot(res);
    }

		let query = {school : schoolId};
		if(req.user.type === CONSTANTS.userTypes.admin){

				ClassService.getAdminClasses(req.user.classes, req.user.id).then((divisionsList)=>{
					res.view('years/years', {
						divisions_list: divisionsList
					});
				}, (err)=>{
						Utils.sendError(res, err);
				});
		}else if(req.user.type === CONSTANTS.userTypes.superuser){


	    DivisionService.getAllDivisions(schoolId).then((divisions)=>{
				var classes = [];
				var classesTemp = req.user.classes;
				if(classesTemp){
						for(var i = 0; i < classesTemp.length; i++){
							classes.push(parseInt(classesTemp[i]));
						}
				}

				Classes.find({id : classes}).exec(function (err, classes) {
					res.view('years/years', {
						classes: classes,
						divisions_list: divisions
					});
				});
	    },(err)=>{
	      Utils.sendError(res, err);
	    });
		}
	},
  getAllDivisions : function(req,res){
    const schoolId = parseInt(req.param('school_id'));
    if(isNaN(schoolId)){
      Utils.redirectToRoot(res);
    }

    DivisionService.getAllDivisions(schoolId).then((divisions)=>{
      res.send(divisions);
    },(err)=>{
      Utils.sendError(res, err);
    });
  },
  renderDivsionNew: function (req, res) {
    const divisionId = req.query.divisionId;


    if(divisionId){
      Divisions.findOne({id : parseInt(divisionId)}).exec((err, obj)=>{
        res.view('years/years_new', {
          "division" : obj
        });
      });
    }else{
      res.view('years/years_new');
    }
	},

	createDivision: function (req, res) {
		Divisions.create({
			name: req.param("name"),
			belongingTo: req.param('school_id'),
			school : parseInt(req.param('school_id')),
			'creator' : req.user.id
		}).exec(function createCB(err, created) {
			if (err) return res.serverError(err);
			else return res.redirect("/schools/" + req.param('school_id') + "/years");
		});
	},
	updateDivison : function(req, res){
		let division = req.body;

		if(!division.id){
			res.status(CONSTANTS.httpCodes.request_missing_param);
			res.send("Division is not provided");
		}

		Divisions.update({id:division.id}, division).exec((err, updatedDocs)=>{
			if(!err & updatedDocs.length > 0){
				res.send(updatedDocs[0]);
			}else if(updatedDocs.length == 0){
				res.status(CONSTANTS.httpCodes.not_found);
				res.send("Division with id " + division.id + " is not found");
			}else{
				Utils.sendError(err);
			}
		});
	},
	delteDivision :function(req,res){
		console.log("Delete division");
		console.log(">>>>>>>>>>>>>>>\t" + req.params.id);
		Divisions.destroy({id : req.params.id}).exec((err)=>{
			if(!err){
				res.send('Division is removed successfully: ' +  req.params.id);
			}else{
				Utils.sendError(res, err);
			}
		})
	},
	getDivisions : function(req, res){
		const schoolId = req.query.schoolId;

		DivisionService.getAllDivisions(schoolId).then((divisions)=>{
			res.send(divisions);
		},(err)=>{
			Utils.sendError(res, err);
		});
	}
};

var getClass = function(division, classIds){
  return new Promise((resolve, reject)=>{
    Classes.find({id : classIds}).exec((err, classes)=>{
      if(!err){
        division = division.toJSON();
        division.classes = classes;
        resolve(division);
      }else{
        reject(err);
      }
    });
  });
};
