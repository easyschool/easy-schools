var mysql      = require('mysql');
const connections = require('./config/connections.js').connections;
const env = process.env.NODE_ENV;

let dbEnv;

switch (env) {
  case 'local':
    dbEnv = connections.local
    break;
  case 'development' :
    dbEnv = connections.development
    break;
  case 'production' :
    dbEnv = connections.production;
    break;
}

console.log(dbEnv);
var connection = mysql.createConnection({
  host     : dbEnv.host,
  user     : dbEnv.user,
  password : dbEnv.password,
  database : dbEnv.database
});

connection.connect();
let script = "INSERT INTO users (username, password, type, firstName,lastName)VALUES   ('marwan', '$2a$10$gbQNFQWf.0NdD75b4b7MzeeVrgB4bcsBqq/9ujSyZEB.NpsLDvLH6', 'superuser', 'Marwan', 'Kahky');"
connection.query(script, function(err, rows, fields) {
  if (!err)
    console.log('The solution is: ', rows);
  else
    console.log('Error while performing Query.');
    console.log(err);
});

connection.end();
